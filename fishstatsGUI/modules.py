# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 17:38:40 2020

@author: Geoffrey Andrews

Modules for use in the graphical version of FishStats

"""

import numpy as np
import re
from scipy import stats
import sys
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import patches as patch
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter as tk
from mpl_toolkits.axes_grid1 import make_axes_locatable
from statsmodels.graphics.gofplots import qqplot


mpl.rcParams['mathtext.default'] = 'regular'

colors = ['#1a1a1a','#666666','#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#a6761d'] #'#e6ab02'             
markers = ['o','v','s','H','D','X','P','^','X','+',]  


def inspectFiles(fishstats,newfiles,*args):
    if 'control' in args:
        files = fishstats.controlfile_paths
    else:
        files = fishstats.datafile_paths
    drugs=[]            
    for i,path_to_datafile in enumerate(newfiles):
        numdrugs=0
        for i,drugname in enumerate(path_to_datafile.split('/')[-1].split('-')[1:]):
            if drugname.isdigit():
                break
            else:
                print("   expecting data for " + drugname)
                numdrugs=numdrugs+1
                drugs.append(drugname)
    return drugs
        
def readDataFiles(fishstats,*args):
    if 'control' in args:
        files = fishstats.controlfile_paths
    else:
        files = fishstats.datafile_paths
    if 'vomit' in args:
        vomit = True
    else:
        vomit=False
  
    alllabels=[]         
    numcontrols = 0
    
    for path_to_datafile in files:
        print("Reading data from " + path_to_datafile)
        platelabels = []
        numdrugs=0
        for i,drugname in enumerate(path_to_datafile.split('/')[-1].split('-')[1:]):
            if drugname.isdigit():
                break
            else:
                print("   expecting data for " + drugname)
                numdrugs=numdrugs+1
        print("   %i drugs expected" % numdrugs)
        
        #group data...hopefully
        datagroups=[]
        for i in np.arange(0,numdrugs*2): #multiply by two for injured/uninjured
            datagroups.append([2*i,2*i+1])
        print("Grouping columns into groups as follows:")
        print(datagroups)
        
        [masterdata,steps] = readDataFile(path_to_datafile,fishstats.starttime,fishstats.endtime,fishstats.rows,fishstats.cols,quiet=True)
        

        timeseries = []       
        period_data = np.zeros((fishstats.rows,fishstats.cols,len(steps)))
    
        for i in np.arange(0, len(steps)):
           # if int(steps[i]['start']) >= starttime and int(steps[i]['end']) <= endtime+1:        
            step = i
            # Read in data    
            period_data[:,:,i] = parseData(masterdata,step,fishstats.rows,fishstats.cols)                  
            timeseries.append(steps[i]['start'])                      
               
            if vomit == True:
                vomitData(masterdata,step) 
        fishstats.timeseries = np.asarray(timeseries)   
        
        # don't bother saving control plate for contour plots (?)
        if 'control' not in args:
            fishstats.platedata.append(np.sum(period_data,axis=2))

        # form groups of data
        grouped_data = np.zeros((fishstats.rows* max(map(len,datagroups)),len(datagroups),len(steps)))

        for i,group in enumerate(datagroups):
            size = len(group)*fishstats.rows
            data = period_data[:,group,:].reshape((size,len(steps)),order='C').T
            grouped_data[:,i,:] = data.T
            
        # add up data to find cumulative value       
        cumulative_plate_data = np.sum(grouped_data,axis=2)

        for i,drugname in enumerate(path_to_datafile.split('/')[-1].split('-')[1:]):
            if drugname.isdigit():
                break
            print("   Adding %s to data..." % drugname)
            if 'fishstats.conditiondata' in locals():
                alldrugs = np.hstack((alldrugs, cumulative_plate_data[:,2*i:2*i+2]))
                alldrugs_timedata = np.hstack((alldrugs_timedata, grouped_data[:,2*i:2*i+2,:]))
            else:
                alldrugs = cumulative_plate_data[:,2*i:2*i+2]
                alldrugs_timedata = grouped_data[:,2*i:2*i+2,:]
            alllabels.append(drugname)
        
        if 'all_data' in locals():
            all_data = np.hstack((all_data,cumulative_plate_data))
        else:
            all_data = cumulative_plate_data
        
        if 'all_data_time' in locals():
            all_data_time = np.dstack((all_data_time,grouped_data))
        else:
            all_data_time = grouped_data
            
    if 'control' in args:
        fishstats.controldata_time = all_data_time
    else:
        fishstats.conditiondata_time = all_data_time
           
    return [alllabels,all_data]              
 
        
def readDataFile(path,starttime,endtime,rows,cols,**kwargs):
    if 'quiet' in kwargs:
        quiet = kwargs.get('quiet')  
    else:    
        quiet = False               
    
    datafile = open(path,encoding='utf-16')
    data = []
    start = -97
    stepcount = 1
    nsteps = 0
    stepdata = {}
    steps = []
    
    datastart = 1e10
    dataend = -1e10
    
    for i,line in enumerate(datafile):
        line = line.replace('\x00', '')
        # Read field headers from first line of file
        if i==0: 
            fields = line.split('\t')
            if len(fields) == 49:
                print("Data file uses type 1 formatting with %i fields" % len(fields))
            elif len(fields) == 56:
                print("Data file uses type 2 formatting with %i fields" % len(fields))
            elif len(fields) == 50:
                if fields[-1] == '\n':
                    print("Data file has 50 fields, though it appears that there is an additional line break character erroneously included in the string. An attempt will be made to remove it.")
                    fields = fields[:-1]                       
            else: 
                print("Data file uses unknown formatting with %i fields. Known data file types have either 49 or 56 fields" % len(fields))
                print(fields)
        # For every other line of file, read in data
        elif len(line.split('\t')) > 2:
            if len(fields) == 49:
                location = line.split('\t')[2]       
                # Reset necessary variables for each step in the data
                if int(float(line.split('\t')[8])) != start:
                    start = float(line.split('\t')[8])  
                    end = float(line.split('\t')[9])
                    if int(start) >= starttime and int(end) <= endtime+1:
                        steps.append({'start':start,'end':end})
                        if stepcount > 1:
                            data.append(stepdata)
                        stepdata = {}
                        stepcount = stepcount+1   
    
                    if start < datastart:
                        datastart = start
                    if end > dataend:
                        dataend = end
                    nsteps = nsteps + 1
                        
                if location not in stepdata:             
                    stepdata[location] ={'start': start,
                                          'end':float(line.split('\t')[9]), 
                                          'inact':int(line.split('\t')[16]), 
                                          'inadist':float(line.split('\t')[18]), 
                                          'smlct':int(line.split('\t')[19]), 
                                          'smldist':float(line.split('\t')[21]), 
                                          'larct':int(line.split('\t')[22]), 
                                          'lardist':float(line.split('\t')[24]) }  
            else:

                location = line.split('\t')[2]       
                # Reset necessary variables for each step in the data
                if int(float(line.split('\t')[8])) != start:
                    start = float(line.split('\t')[8])  
                    end = float(line.split('\t')[9])
                    if int(start) >= starttime and int(end) <= endtime+1:
                        steps.append({'start':start,'end':end})
                        if stepcount > 1:
                            data.append(stepdata)
                        stepdata = {}
                        stepcount = stepcount+1   
    
                    if start < datastart:
                        datastart = start
                    if end > dataend:
                        dataend = end
                    nsteps = nsteps + 1
                        
                   
            if location not in stepdata:             
                stepdata[location] ={'start': start,
                                      'end':float(line.split('\t')[9]), 
                                      'inact':int(line.split('\t')[22]), 
                                      'inadist':float(line.split('\t')[24]), 
                                      'smlct':int(line.split('\t')[25]), 
                                      'smldist':float(line.split('\t')[27]), 
                                      'larct':int(line.split('\t')[28]), 
                                      'lardist':float(line.split('\t')[30]) }  

    data.append(stepdata)
    if quiet==False:
        print("   There are %i fields in the file:" % len(fields))
        for j in np.arange(0,len(fields)):
            print("      %i - " % j + fields[j])
        print("   File spans %i - %i s with %i steps detected" % (datastart,dataend, nsteps))
        print("   Selected time range is %i - %i s (%i time steps)" % (steps[0]['start'],steps[-1]['end'],stepcount-1))
    datafile.close    
    return [data,steps]


def parseData(data,step,fish_per_cond,conds):
    data_out = np.zeros([fish_per_cond,conds])
   # print(data[step])
    for fish in data[step]:
        # divide fish ID number by number of fish per condition and round 
        # to get fish number within condition
  
        row = int((int(re.findall(r'\d+', fish)[0])-1)/conds)
        col = (int(re.findall(r'\d+', fish)[0])-1)%conds + 1  
        
        ncols = int(12/conds) # number of columns to include in each condition
        
        fish_num = row + (col+1)%ncols*8
        cond = int((col+1)/ncols)-1
        data_out[fish_num, cond-1] = (data[step][fish]['inadist'] + 
                                       data[step][fish]['smldist'] + 
                                       data[step][fish]['lardist'])
    if np.product(np.shape(data_out)) - conds*fish_per_cond !=0:
        sys.exit("%i fish have been counted in the file but %i are expected" %(np.product(np.shape(data_out)),conds*fish_per_cond))
    return data_out

def poolData(fishstats,*args):
    print("Pooling all conditions and controls with identical names...")
    pooledconditionnames = []
    poolconditioncount = []
    pooledconditiondata = []
    pooledconditiondata_time = []
    for i,icond in enumerate(fishstats.plotconditions):
        condition = fishstats.conditionnames[icond]
        if condition not in pooledconditionnames:
            print("adding %s to pool" % condition)
            pooledconditionnames.append(condition)
            poolconditioncount.append(1)
            pooledconditiondata.append(fishstats.conditiondata[:,icond])
            pooledconditiondata_time.append(fishstats.conditiondata_time[:,:,icond])
        else:
            print("condition %s is preexisting, appending data" % condition)
            imatch = pooledconditionnames.index(condition)
            poolconditioncount[imatch] = poolconditioncount[imatch] + 1
            pooledconditiondata[imatch] = np.hstack((pooledconditiondata[imatch],fishstats.conditiondata[:,icond]))            
            pooledconditiondata_time[imatch] = np.vstack((pooledconditiondata_time[imatch],fishstats.conditiondata_time[:,:,icond]))
          

    print("Final list of conditions to plot:")

    for icond, name in enumerate(pooledconditionnames):
        print("   %s (x%i) - %i fish" %(name, poolconditioncount[icond], np.shape(pooledconditiondata[icond])[0]))
  
    pooledconditiondata_array = -1*np.ones((len(pooledconditiondata),max(len(pooledconditiondata[i]) for i in np.arange(0,len(pooledconditiondata)))))
    pooledconditiondata_time_array = -1*np.ones((len(pooledconditiondata_time),max(len(pooledconditiondata_time[i]) for i in np.arange(0,len(pooledconditiondata))),np.shape(fishstats.conditiondata_time)[-1]))
    for i,row in enumerate(pooledconditiondata):
        for j,elem in enumerate(row):
            pooledconditiondata_array[i,j] = elem
            for k,tp in enumerate(pooledconditiondata_time[i][j,:]):
                pooledconditiondata_time_array[i,j,k] = tp
    pooledconditiondata = np.asarray(pooledconditiondata)
    pooledconditiondata_time_array = np.swapaxes(pooledconditiondata_time_array,0,1)
    
    if len(fishstats.controls) > 0:
        print("Final list of controls to plot:")
        pooledcontrolnames = []
        poolcontrolcount = []
        pooledcontroldata = []
        pooledcontroldata_time = []
        for i, icond in enumerate(fishstats.plotcontrols):
            control = fishstats.controlnames[icond]
            if control not in pooledcontrolnames:
                pooledcontrolnames.append(control)
                poolcontrolcount.append(1)
                pooledcontroldata.append(fishstats.controldata[:,icond])
                pooledcontroldata_time.append(fishstats.controldata_time[:,:,icond])

            else:
                imatch = pooledcontrolnames.index(control)
                poolcontrolcount[imatch] = poolcontrolcount[imatch] + 1
                pooledcontroldata[imatch] = np.hstack((pooledcontroldata[imatch],fishstats.controldata[:,icond]))
                pooledcontroldata_time[imatch] = np.vstack((pooledcontroldata_time[imatch],fishstats.controldata_time[:,:,icond]))
        for icond, name in enumerate(pooledcontrolnames):
            print("   %s (x%i) - %i fish" %(name, poolcontrolcount[icond], np.shape(pooledcontroldata[icond])[0]))
        pooledcontroldata_array = -1*np.ones((len(pooledcontroldata),max(len(pooledcontroldata[i]) for i in np.arange(0,len(pooledcontroldata)))))
        pooledcontroldata_time_array = -1*np.ones((len(pooledcontroldata),max(len(pooledcontroldata[i]) for i in np.arange(0,len(pooledcontroldata))),np.shape(fishstats.controldata_time)[-1]))
        
        
        for i,row in enumerate(pooledcontroldata):
            for j,elem in enumerate(row):
                pooledcontroldata_array[i,j] = elem
                for k,tp in enumerate(pooledcontroldata_time[i][j,:]):
                    pooledcontroldata_time_array[i,j,k] = tp

        pooledcontroldata = np.asarray(pooledcontroldata)
        pooledcontroldata_time_array = np.swapaxes(pooledcontroldata_time_array,0,1)

    else:
        pooledcontroldata_array = -1*np.ones((0,0))
        pooledcontroldata_time_array = -1*np.ones((0,0,0))
        pooledcontrolnames = []
        
    fishstats.conditiondata = np.ma.masked_less_equal(pooledconditiondata_array,-1).T #pooledconditiondata.T
    fishstats.conditionnames = pooledconditionnames
    fishstats.controldata = np.ma.masked_less_equal(pooledcontroldata_array,-1).T #pooledcontroldata.T
    fishstats.controlnames = pooledcontrolnames
    fishstats.controls = [i for i in np.arange(0,len(pooledcontrolnames))]
    fishstats.conditions = [i for i in np.arange(0,len(pooledconditionnames))]
    fishstats.plotcontrols=fishstats.controls
    fishstats.plotconditions=fishstats.conditions
    fishstats.conditiondata_time = np.ma.masked_less_equal(pooledconditiondata_time_array,-1)
    fishstats.controldata_time = np.ma.masked_less_equal(pooledcontroldata_time_array,-1)
   

def cleanData(fishstats, *args):
    
    good_data = np.ma.array(fishstats.conditiondata)
    good_controls = np.ma.array(fishstats.controldata)
            
    if 'remove2sd' in args:
        # Remove anything greater than 2 standard deviations away
        print(r"Removing data greater than 2 stdev away from mean")
        for i in fishstats.conditions:
            print("Condition %i name: %s" % (i,fishstats.conditionnames[i]))
         #   print("  Group %i (Category: '%s')" % (i+1, labels[i]))
            print("    Mean: %3.2f; stdev= %3.2f" % (fishstats.conditionstats_raw['mean_vals'][i], fishstats.conditionstats_raw['stdev'][i]))
            good_data[:,i] = np.ma.masked_outside(fishstats.conditiondata[:,i], 
                                         fishstats.conditionstats_raw['mean_vals'][i] - fishstats.conditionstats_raw['stdev'][i]*2,
                                         fishstats.conditionstats_raw['mean_vals'][i] + fishstats.conditionstats_raw['stdev'][i]*2)
            print("    %i of %i values are within the range %3.2f - %3.2f" % (good_data[:,i].count(), len(fishstats.conditiondata[:,i]),fishstats.conditionstats_raw['mean_vals'][i] - fishstats.conditionstats_raw['stdev'][i]*2,
                                         fishstats.conditionstats_raw['mean_vals'][i] + fishstats.conditionstats_raw['stdev'][i]*2))
            print("    There are %i zero values." % (len(fishstats.conditiondata[:,i])-np.count_nonzero(fishstats.conditiondata[:,i])))
        print("Removing data from control condition greater than 2stdev away from mean")
        for i in fishstats.controls:
            print("Control condition %i: %s" % (i,fishstats.controlnames[i]))
            print("    Mean: %3.2f; stdev= %3.2f" % (fishstats.controlstats_raw['mean_vals'][i], fishstats.controlstats_raw['stdev'][i]))
            good_controls[:,i] = np.ma.masked_outside(fishstats.controldata[:,i], 
                                         fishstats.controlstats_raw['mean_vals'][i] - fishstats.controlstats_raw['stdev'][i]*2,
                                         fishstats.controlstats_raw['mean_vals'][i] + fishstats.controlstats_raw['stdev'][i]*2)
            print("    %i of %i values are within the range %3.2f - %3.2f" % (good_controls[:,i].count(), len(fishstats.controldata[:,i]),fishstats.controlstats_raw['mean_vals'][i] - fishstats.controlstats_raw['stdev'][i]*2,
                                         fishstats.controlstats_raw['mean_vals'][i] + fishstats.controlstats_raw['stdev'][i]*2))
            print("    There are %i zero values." % (len(fishstats.controldata[:,i])-np.count_nonzero(fishstats.controldata[:,i])))
            
    # Remove 0 values
    if 'remove0' in args:
        print("Removing all zero values from data")
        for i in np.arange(0,np.shape(fishstats.conditiondata)[-1]):
            good_data[:,i] = np.ma.masked_equal(good_data[:,i],0)
        for i in np.arange(0,np.shape(fishstats.controldata)[-1]):
            good_controls[:,i] = np.ma.masked_equal(good_controls[:,i],0)
            
    fishstats.conditiondata_clean = good_data
    fishstats.controldata_clean = good_controls  

def doStats(data,**kwargs): 
    if 'control' in kwargs:
        control = kwargs.get('control')  
    else:
        control = data   
    if 'normtest' in kwargs:
        normtest = bool(kwargs.get('normtest'))
    else:
        normtest=False 
    if 'multi' in kwargs:
        multi = bool(kwargs.get('multi'))
    else:
        multi = False
    if 'nocontrol' in kwargs:
        nocontrol = bool(kwargs.get('nocontrol'))
    else:
        nocontrol = False
    mean_vals = np.mean(data,axis=0)
    stderror = stats.mstats.sem(data,axis=0)
    stdev = np.std(data,axis=0)
    
    numgroups=np.shape(data)[-1]
    Tvals = np.zeros(numgroups)
    pvals = np.zeros(numgroups)
    Fvals = np.zeros(numgroups)
    pvals_ANOVA = np.zeros(numgroups)
    shapiro = np.zeros(numgroups)
    pvals_shapiro = np.zeros(numgroups)
    if nocontrol == False:
      
  
        for col in np.arange(0, numgroups):
            
            if multi == True:
                if numgroups > 1:
                    if col%2 == 0:
                        [Tvals[col], pvals[col]] = [0,0]
                        [Fvals[col], pvals_ANOVA[col]] = [0,0]
                    else:
                        [Tvals[col], pvals[col]] = stats.mstats.ttest_ind(data[:,col],control[:,col-1])
                        [Fvals[col], pvals_ANOVA[col]] = stats.mstats.f_oneway(data[:,col],control[:,col-1])  
                else:
    
                    [Tvals[col], pvals[col]] = stats.mstats.ttest_ind(data[:,col],control[:,col])
                    [Fvals[col], pvals_ANOVA[col]] = stats.mstats.f_oneway(data[:,col],control[:,col])                  
            else:
               if col%2 ==0 or col==1: # case for uninjured fish (even columns)
                    [Tvals[col], pvals[col]] = stats.mstats.ttest_ind(data[:,col],control[:,0])
                    [Fvals[col], pvals_ANOVA[col]] = stats.mstats.f_oneway(data[:,col],control[:,0])
               else: # case for injured fish (odd columns)   
                    [Tvals[col], pvals[col]] = stats.mstats.ttest_ind(data[:,col],control[:,1])                      
                    [Fvals[col], pvals_ANOVA[col]] = stats.mstats.f_oneway(data[:,col],control[:,1])
               
                
                
            if normtest:
                mirrored_data = np.hstack((-data[col],data[col]))
                [shapiro[col],pvals_shapiro[col]] = stats.shapiro(data[col])
                fig = plt.figure(figsize=(8.5,3.5))
                ax1 = fig.add_subplot(121)
                y,x,_ =ax1.hist(data[data[:,col]!=0,col],color=colors[col])
                xvals = np.linspace(plt.gca().get_xlim()[0],plt.gca().get_xlim()[1],100)
                mu,std=stats.norm.fit(data[col])
                ax1.plot(xvals,stats.norm.pdf(xvals,mu,std),'k')
             
                ax2 = fig.add_subplot(122)
                ax2.axis('equal')
                qqplot(data[col],line='s',ax=ax2,color=colors[col])
                fig.tight_layout()
                title=fig.suptitle(labels[col] + "\nShapiro-Wilk test: %3.2f; p:%3.2e" % (shapiro[col],pvals_shapiro[col]),
                             y=1.1)
                figname = path_to_output + "/" + labels[col] + "_normtest.png"
              #  fig.savefig(figname,dpi=400,bbox_include_extra_artists=(title), bbox_inches='tight')

    return {'mean_vals':mean_vals, 
            'stdev':stdev, 
            'stderror': stderror, 
            'Tvals':Tvals, 
            'pvals':pvals,
            'Fvals':Fvals, 
            'pvals_ANOVA':pvals_ANOVA}  
        
def writeData(fishstats,*args):
    # output data to text files 
    
    if np.shape(fishstats.conditiondata)[0] > 2*fishstats.rows:
        poolstring = "_pool"
    else:
        poolstring = ""
    
  #  data = np.hstack((fishstats.controldata[:,fishstats.plotcontrols],fishstats.conditiondata[:,fishstats.plotconditions]))
    data = -1*np.ones((max(np.shape(fishstats.controldata[:,fishstats.plotcontrols])[0],
                          np.shape(fishstats.conditiondata[:,fishstats.plotconditions])[0]), np.shape(fishstats.controldata[:,fishstats.plotcontrols])[1]+ np.shape(fishstats.conditiondata[:,fishstats.plotconditions])[1]))
    colcount = 0
    for j in np.arange(0,np.shape(fishstats.controldata[:,fishstats.plotcontrols])[1]):
        for i in np.arange(0,np.shape(fishstats.controldata[:,fishstats.plotcontrols])[0]):
            data[i,colcount] = fishstats.controldata[i,j]
        colcount = colcount + 1
    for j in np.arange(0,np.shape(fishstats.conditiondata[:,fishstats.plotconditions])[1]):
        for i in np.arange(0,np.shape(fishstats.conditiondata[:,fishstats.plotconditions])[0]):
            data[i,colcount] = fishstats.conditiondata[i,j]
        colcount = colcount + 1

    labels = (list([fishstats.controlnames[i] for i in list(fishstats.plotcontrols)]) + 
                 list([fishstats.conditionnames[i] for i in list(fishstats.plotconditions)]))    
    
    controlconds = np.shape(fishstats.plotcontrols)[-1]
    dataconds = np.shape(fishstats.plotconditions)[-1]    
    conds= controlconds+dataconds
    


    filename = (fishstats.outputpath[:-1] + "_" + 
                str(fishstats.starttime).zfill(4) + "-" + 
                str(fishstats.endtime).zfill(4) + poolstring + ".txt") 
    print("Printing file to %s" % filename)
    outfile = open(filename,'w+')    
    outfile.write((5*" <><" + (" FishStats v%s " % fishstats.version) + 5*"><> ").center(conds*11+7, '~')) 
    outfile.write("\n")
    outfile.write("Total swimming distances (inadist + smldist + lardist) in mm\n")
    outfile.write("Time period: start=%i s, end=%i s\n" % (fishstats.starttime,fishstats.endtime) )
    outfile.write("Conditions are as follows:\n")
    for ii in np.arange(0,conds):
        outfile.write(("Cndtn" + str(ii+1).zfill(2)) +": " + labels[ii].strip() + "\n")
    outfile.write("<>< ".ljust(conds*11+7,'~'))
    outfile.write("\nCONDTN:   ")
    for ii in np.arange(0,conds):
        outfile.write(("Cndtn" + str(ii+1).zfill(2)).ljust(11))
    outfile.write("\n")
    outfile.write(" ><>".rjust(conds*11+7,'~'))
    outfile.write("\n")
   
    outfile.write(" Period Stats (no cleaning) ".center(conds*11+7,"=")) 
    outfile.write("\nMEANVL: ")            
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_raw['mean_vals'][fishstats.plotcontrols],fishstats.conditionstats_raw['mean_vals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("STDERR: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_raw['stderror'][fishstats.plotcontrols],fishstats.conditionstats_raw['stderror'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("STDDEV: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_raw['stdev'][fishstats.plotcontrols],fishstats.conditionstats_raw['stdev'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("T-test".center(conds*11+7,'_')) 

    outfile.write("\n")
    outfile.write("TVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_raw['Tvals'][fishstats.plotcontrols],fishstats.conditionstats_raw['Tvals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_raw['pvals'][fishstats.plotcontrols],fishstats.conditionstats_raw['pvals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("ANOVA".center(conds*11+7,'_')) 
    outfile.write("\n")
    outfile.write("FVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_raw['Fvals'][fishstats.plotcontrols],fishstats.conditionstats_raw['Fvals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_raw['pvals_ANOVA'][fishstats.plotcontrols],fishstats.conditionstats_raw['pvals_ANOVA'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
#    
    outfile.write("Period Stats (cleaned)".center(conds*11+7,'=')) 
    outfile.write("\nMEANVL: ")            
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_clean['mean_vals'][fishstats.plotcontrols],fishstats.conditionstats_clean['mean_vals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("STDERR: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_clean['stderror'][fishstats.plotcontrols],fishstats.conditionstats_clean['stderror'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("STDDEV: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_clean['stdev'][fishstats.plotcontrols],fishstats.conditionstats_clean['stdev'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("T-test".center(conds*11+7,'_')) 
    outfile.write("\n")
    outfile.write("TVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_clean['Tvals'][fishstats.plotcontrols],fishstats.conditionstats_clean['Tvals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_clean['pvals'][fishstats.plotcontrols],fishstats.conditionstats_clean['pvals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")

    outfile.write("ANOVA".center(conds*11+7,'_')) 
    outfile.write("\n")
    outfile.write("FVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_clean['Fvals'][fishstats.plotcontrols],fishstats.conditionstats_clean['Fvals'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(np.hstack((fishstats.controlstats_clean['pvals_ANOVA'][fishstats.plotcontrols],fishstats.conditionstats_clean['pvals_ANOVA'][fishstats.plotconditions])), [1,conds]), fmt="%10.6f")
    
    outfile.write("Detailed Values".center(conds*11+7,'='))
    outfile.write("\n")
    for ii, row in enumerate(data):
        outfile.write("FISH_"+str(ii+1).zfill(2)+":")
        np.savetxt(outfile,np.reshape(row, [1,conds]), fmt="%10.6f")
    outfile.close()
    
def writeDataSimple(fishstats, *args):
    # output data to text files 
    if np.shape(fishstats.conditiondata)[0] > 2*fishstats.rows:
        poolstring = "_pool"
    else:
        poolstring = ""

    data =fishstats.conditiondata[:,fishstats.plotconditions]

    labels = list([fishstats.conditionnames[i] for i in list(fishstats.plotconditions)])
    
    dataconds = np.shape(fishstats.plotconditions)[-1]    
    conds= dataconds

    filename = (fishstats.outputpath[:-1] + "_" + 
                str(fishstats.starttime).zfill(4) + "-" + 
                str(fishstats.endtime).zfill(4) + poolstring + ".txt") 
    print("Printing file to %s" % filename)
    outfile = open(filename,'w+')    
    outfile.write((5*" <><" + (" FishStats v%s " % fishstats.version) + 5*"><> ").center(conds*11+7, '~')) 
    outfile.write("\n")
    outfile.write("Total swimming distances (inadist + smldist + lardist) in mm\n")
    outfile.write("Time period: start=%i s, end=%i s\n" % (fishstats.starttime,fishstats.endtime) )
    outfile.write("Conditions are as follows:\n")
    for ii in np.arange(0,conds):
        outfile.write(("Cndtn" + str(ii+1).zfill(2)) +": " + labels[ii].strip() + "\n")
    outfile.write("<>< ".ljust(conds*11+7,'~'))
    outfile.write("\nCONDTN:   ")
    for ii in np.arange(0,conds):
        outfile.write(("Cndtn" + str(ii+1).zfill(2)).ljust(11))
    outfile.write("\n")
    outfile.write(" ><>".rjust(conds*11+7,'~'))
    outfile.write("\n")
   
    outfile.write(" Period Stats (no cleaning) ".center(conds*11+7,"=")) 
    outfile.write("\nMEANVL: ")            
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_raw['mean_vals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("STDERR: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_raw['stderror'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("STDDEV: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_raw['stdev'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("T-test".center(conds*11+7,'_')) 

    outfile.write("\n")
    outfile.write("TVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_raw['Tvals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_raw['pvals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("ANOVA".center(conds*11+7,'_')) 
    outfile.write("\n")
    outfile.write("FVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_raw['Fvals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_raw['pvals_ANOVA'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
#    
    outfile.write("Period Stats (cleaned)".center(conds*11+7,'=')) 
    outfile.write("\nMEANVL: ")            
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_clean['mean_vals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("STDERR: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_clean['stderror'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("STDDEV: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_clean['stdev'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("T-test".center(conds*11+7,'_')) 
    outfile.write("\n")
    outfile.write("TVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_clean['Tvals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_clean['pvals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")

    outfile.write("ANOVA".center(conds*11+7,'_')) 
    outfile.write("\n")
    outfile.write("FVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_clean['Fvals'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(fishstats.conditionstats_clean['pvals_ANOVA'][fishstats.plotconditions], [1,conds]), fmt="%10.6f")
    
    outfile.write("Detailed Values".center(conds*11+7,'='))
    outfile.write("\n")
    for ii, row in enumerate(data):
        outfile.write("FISH_"+str(ii+1).zfill(2)+":")
        np.savetxt(outfile,np.reshape(row, [1,conds]), fmt="%10.6f")
    outfile.close()    
    
    
def plotData(fishstats,*args, ):
    if 'multi' in args:
        multi = True
    else:
        multi = False
    if np.shape(fishstats.conditiondata)[0] > 2*fishstats.rows:
        poolstring = "_pool"
        pool = True
    else:
        poolstring = ""
        pool = False
        
    space_multiplier = 1.75
    crit1 = False
    crit2 = False
    
    ngroups = len(fishstats.controlnames)+ len(fishstats.conditionnames)
    xlimit = len(fishstats.plotconditions) + len(fishstats.plotcontrols) 
    maxval = 0

    plotwindow = tk.Toplevel()    
    fig = plt.Figure(figsize = (space_multiplier*ngroups/2, 8))
    
    
    canvas = FigureCanvasTkAgg(fig,master=plotwindow)
    ax1 = fig.add_subplot(111)

    if multi == False:
        plotlabels = (list([fishstats.controlnames[i] for i in list(fishstats.plotcontrols)]) + 
                      list([fishstats.conditionnames[i] for i in list(fishstats.plotconditions)]))

        for ix,i in enumerate(fishstats.plotcontrols):
            print("Plotting the following control condition:")
            print(fishstats.controlnames[i])
            ax1.bar(space_multiplier*(ix+1), fishstats.controlstats_clean['mean_vals'][i],yerr=fishstats.controlstats_clean['stderror'][i], width = 0.8,
                    edgecolor='k', 
                    color=[((i+1)/(len(fishstats.controlnames)+1),(i+1)/(len(fishstats.controlnames)+1),(i+1)/(len(fishstats.controlnames)+1),1.0)],
                    error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))
            if 'plotfish' in args:
                ax1.plot(space_multiplier*(int(ix+1) + (np.random.rand(np.shape(fishstats.controldata_clean)[0],1)-0.5)*0.75), 
                    fishstats.controldata_clean[:,i],markers[(ix)%len(markers)],color=colors[ix%len(colors)],markersize=4)
        for ix,i in enumerate(fishstats.plotconditions):
            crit1 = False
            crit2 = False
            print(("Plotting condition %i," %i)+fishstats.conditionnames[i])
            ax1.bar(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1), fishstats.conditionstats_clean['mean_vals'][i],yerr=fishstats.conditionstats_clean['stderror'][i], width = 0.8,
                        edgecolor='k', 
                        color=['none' if (i-2)%2==0 else (0.9,0.9,0.9,1.0)],
                        error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))
            if 'plotfish' in args:
                ax1.plot(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1) + (np.random.rand(np.shape(fishstats.controldata_clean)[0],1)-0.5)*0.75, 
                         fishstats.conditiondata_clean[:,i],markers[(ix+2)%len(markers)],color=colors[(ix+2)%len(colors)],markersize=4)
                maxval = max(max(np.amax(fishstats.controldata_clean,axis=1)),max(np.amax(fishstats.conditiondata_clean,axis=1)))
                ylimit = 1.25*maxval
                ymarker = 1.1*np.max(fishstats.conditiondata_clean[:,i])

            else:
                maxval = max(max(fishstats.controlstats_clean['mean_vals'][fishstats.plotcontrols]+
                                 fishstats.controlstats_clean['stderror'][np.argmax(fishstats.controlstats_clean['mean_vals'][fishstats.plotcontrols])]),
                             max(fishstats.conditionstats_clean['mean_vals'][fishstats.plotconditions])+
                                 fishstats.conditionstats_clean['stderror'][np.argmax(fishstats.conditionstats_clean['mean_vals'][fishstats.plotconditions])])    
                ylimit=1.25*maxval
                ymarker = 1.1*(fishstats.conditionstats_clean['mean_vals'][i]+fishstats.conditionstats_clean['stderror'][i])
            if i%2==1:
                if fishstats.conditionstats_clean['pvals'][i] < 0.05 and fishstats.conditionstats_clean['mean_vals'][i] > fishstats.controlstats_clean['mean_vals'][i%2]:
                    crit1 = True
                    print("Criterion 1 met (pval < 0.05 and mean value of injured drug condition > mean value of injured control condition) ).")
 #           if fishstats.conditionstats_clean['mean_vals'][i]/fishstats.controlstats_clean['mean_vals'][0] >= 0.8 and i%2==1:
                if fishstats.conditionstats_clean['mean_vals'][i]/fishstats.conditionstats_clean['mean_vals'][i-1] >= 0.8:
                    crit2 = True
                    print("Criterion 2 met (mean value of injured condition >= 80% of uninjured condition).")               
            if crit1 == True and crit2 == True:
                    ax1.annotate("!",(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1),ymarker),ha='center',fontsize=12,weight='bold')
                    circle1 = mpl.patches.Ellipse((space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1)+0.01,ymarker+0.1/(space_multiplier*(xlimit+1)/ylimit)), width=0.4, height=0.4/(space_multiplier*(xlimit+1)/ylimit), color='k',fill=False)
                    ax1.add_patch(circle1)
                    print(">>> Potential hit found! Plot will be marked with an '!'.")
            elif crit1 == True:
                    print("Plot will be marked with an '*'.")
                    ax1.annotate("*",(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1),ymarker),ha='center',fontsize=12,weight='bold')
            elif crit2 == True:
                    print("Plot will be marked with an '#'.")
                    ax1.annotate("#",(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1),ymarker),ha='center',fontsize=12,weight='bold')
                    crit2 = True               
    else:
        test=[list([fishstats.controlnames[i] for i in list(fishstats.plotcontrols)]), list([fishstats.conditionnames[i] for i in list(fishstats.plotconditions)])]
        plotlabels = [test[i%2][int(i/2)] for i in np.arange(0,len(list([fishstats.controlnames[i] for i in list(fishstats.plotcontrols)]) + list([fishstats.conditionnames[i] for i in list(fishstats.plotconditions)])))]
        for ix,i in enumerate(fishstats.plotconditions):
            crit1 = False
            crit2 = False
            print("Plotting condition %i: %2s" % (i,fishstats.controlnames[i]))
            print("Plotting condition %i: %2s" % (i,fishstats.conditionnames[i]))
            ax1.bar(space_multiplier*(2*ix+1), fishstats.controlstats_clean['mean_vals'][i-1],yerr=fishstats.controlstats_clean['stderror'][i-1], width = 0.8,
                    edgecolor='k', 
                    color=[(0.3,0.3,0.3,1.0)],
                    error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))
            if 'plotfish' in args:
                ax1.plot(space_multiplier*(int(ix+1) + (np.random.rand(np.shape(fishstats.controldata_clean)[0],1)-0.5)*0.5), 
                    fishstats.controldata_clean[:,i-1],markers[(ix)%len(markers)],color=colors[ix%len(colors)],markersize=4)
            ax1.bar(space_multiplier*(2*ix+2), fishstats.conditionstats_clean['mean_vals'][i],yerr=fishstats.conditionstats_clean['stderror'][i], width = 0.8,
                        edgecolor='k', 
                        color=[(0.9,0.9,0.9,1.0)],
                        error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))
            if fishstats.conditionstats_clean['pvals'][i] < 0.05 and fishstats.conditionstats_clean['mean_vals'][i] > fishstats.controlstats_clean['mean_vals'][i%2] and i%2==1:
                    crit1 = True
                    print("Criterion 1 met (pval < 0.05 and mean value of injured drug condition > mean value of injured control condition) ).")
            if fishstats.conditionstats_clean['mean_vals'][i]/fishstats.controlstats_clean['mean_vals'][0] >= 0.8 and i%2==1:
                    crit2 = True
                    print("Criterion 2 met (mean value of injured condition >= 80% of uninjured condition).")      
            if 'plotfish' in args:
                ax1.plot(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1) + (np.random.rand(np.shape(fishstats.controldata_clean)[0],1)-0.5)*0.55, 
                         fishstats.conditiondata_clean[:,i],markers[(ix+2)%len(markers)],color=colors[(ix+2)%len(colors)],markersize=4)
                maxval = max(max(np.amax(fishstats.controldata_clean,axis=1)),max(np.amax(fishstats.conditiondata_clean,axis=1)))
                ylimit = 1.25*maxval
                ymarker = 1.1*np.max(fishstats.conditiondata_clean[:,i])
            else:
                maxval = max(max(fishstats.controlstats_clean['mean_vals'][fishstats.plotcontrols]+
                                 fishstats.controlstats_clean['stderror'][np.argmax(fishstats.controlstats_clean['mean_vals'][fishstats.plotcontrols])]),
                             max(fishstats.conditionstats_clean['mean_vals'][fishstats.plotconditions])+
                                 fishstats.conditionstats_clean['stderror'][np.argmax(fishstats.conditionstats_clean['mean_vals'][fishstats.plotconditions])])    
                ylimit=1.25*maxval
                ymarker = 1.1*(fishstats.conditionstats_clean['mean_vals'][i]+fishstats.conditionstats_clean['stderror'][i])
            if crit1 == True and crit2 == True:
                    ax1.annotate("!",(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1),ymarker),ha='center',fontsize=12,weight='bold')
                    circle1 = mpl.patches.Ellipse((space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1)+0.01,ymarker+0.1/(space_multiplier*(xlimit+1)/ylimit)), width=0.4, height=0.4/(space_multiplier*(xlimit+1)/ylimit), color='k',fill=False)
                    ax1.add_patch(circle1)
                    print(">>> Potential hit found! Plot will be marked with an '!'.")
            elif crit1 == True:
                    print("Plot will be marked with an '*'.")
                    ax1.annotate("*",(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1),ymarker),ha='center',fontsize=12,weight='bold')
            elif crit2 == True:
                    print("Plot will be marked with an '#'.")
                    ax1.annotate("#",(space_multiplier*(int(ix)+len(fishstats.plotcontrols)+1),ymarker),ha='center',fontsize=12,weight='bold')
                    crit2 = True  

    ax1.set_ylim([0,ylimit])
    ax1.set_xlim([0,space_multiplier*(xlimit+1)])
    ax1.set_xticks(space_multiplier*(np.arange(1,len(fishstats.plotcontrols)+len(fishstats.plotconditions)+1)))
    xticks = ax1.set_xticklabels(plotlabels ,rotation=60,fontsize=fishstats.fontsize)
    ax1.set_title("Time Period: (%i s - %i s)" % (fishstats.starttime, fishstats.endtime),fontsize=fishstats.fontsize)
    ax1.set_ylabel("Cumulative Distance (cm)",fontsize=fishstats.fontsize)
#    figname = path_to_output + "/" + path_to_output.split('/')[-1]  +"_" + str(start).zfill(4) + "-" + str(end).zfill(4) + ".png"
    fig.tight_layout()
    figname = fishstats.outputpath[:-1] +"_columnplot_" + str(fishstats.starttime).zfill(4) + "-" + str(fishstats.endtime).zfill(4) + poolstring + ".png"
    print("Saving figure to " + figname)
    fig.savefig(figname,dpi=500,bbox_extra_artists=(xticks), bbox_inches='tight')
   ## plt.show()
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    
    
def plotDataSimple(fishstats,*args):
    if 'multi' in args:
        multi = True
    else:
        multi = False
    if np.shape(fishstats.conditiondata)[0] > 2*fishstats.rows:
        poolstring = "_pool"
    else:
        poolstring = ""
    space_multiplier = 1.75
    
    ngroups = len(fishstats.controlnames)+ len(fishstats.conditionnames)
    xlimit = len(fishstats.plotconditions) + len(fishstats.plotcontrols) 

    plotwindow = tk.Toplevel()    
    fig = plt.figure(figsize = (space_multiplier*ngroups/2, 8))
    canvas = FigureCanvasTkAgg(fig,master=plotwindow)
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1.0)
    ax1 = fig.add_subplot(111)

    plotlabels = fishstats.conditionnames
    for ix,i in enumerate(fishstats.plotconditions):
        print(("Plotting condition %i," %i)+fishstats.conditionnames[i])
        ax1.bar(space_multiplier*int(ix+1), fishstats.conditionstats_clean['mean_vals'][i],yerr=fishstats.conditionstats_clean['stderror'][i], width = 0.8,
                    edgecolor='k', 
                    color=['none' if (i-2)%2==0 else (0.9,0.9,0.9,1.0)],
                    error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))
        offset = 0.1*(fishstats.conditionstats_clean['mean_vals'][i]+fishstats.conditionstats_clean['stderror'][i])
        if 'plotfish' in args:
            ax1.plot(space_multiplier*(int(ix+1) + (np.random.rand(np.shape(fishstats.conditiondata_clean)[0],1)-0.5)*0.55), 
                     fishstats.conditiondata_clean[:,i],markers[(ix+2)%len(markers)],color=colors[(ix+2)%len(colors)],markersize=4)
       #     ax1.plot(space_multiplier*(int(ix+1) + np.zeros(np.shape(fishstats.conditiondata_clean)[0],1)), 
        #             fishstats.conditiondata_clean[:,i],markers[ix+2],color=colors[ix+2],markersize=4)
    maxval = max(fishstats.conditionstats_clean['mean_vals'][fishstats.plotconditions])   
    ax1.set_ylim([0,1.5*maxval])
    ax1.set_xlim([0,space_multiplier*(xlimit+1)])
    ax1.set_xticks(space_multiplier*(np.arange(1,len(fishstats.plotcontrols)+len(fishstats.plotconditions)+1)))
    xticks = ax1.set_xticklabels(plotlabels ,rotation=60,fontsize=fishstats.fontsize)
    ax1.set_title("Time Period: (%i s - %i s)" % (fishstats.starttime, fishstats.endtime),fontsize=fishstats.fontsize)
    ax1.set_ylabel("Cumulative Distance (cm)",fontsize=fishstats.fontsize)
#    figname = path_to_output + "/" + path_to_output.split('/')[-1]  +"_" + str(start).zfill(4) + "-" + str(end).zfill(4) + ".png"
    fig.tight_layout()
    figname = fishstats.outputpath[:-1] +"_columnplot_" + str(fishstats.starttime).zfill(4) + "-" + str(fishstats.endtime).zfill(4) + poolstring + ".png"
    print("Saving figure to " + figname)
    fig.savefig(figname,dpi=400,bbox_extra_artists=(xticks), bbox_inches='tight')
   ## plt.show()
    canvas.draw()    

def plotTimeSeries(fishstats,aspect,*args,**kwargs):
    if "nocontrol" in args:
        nocontrol = True
    else:
        nocontrol = False
    if np.shape(fishstats.conditiondata)[0] > 2*fishstats.rows:
        poolstring = "_pool"
    else:
        poolstring = ""

 # good_time_data = fishstats.conditiondata_time
    time = fishstats.timeseries
    labels = (list([fishstats.controlnames[i] for i in list(fishstats.plotcontrols)]) + 
                 list([fishstats.conditionnames[i] for i in list(fishstats.plotconditions)]))       

    conditionmask = np.ma.getmask(fishstats.conditiondata_clean[:,fishstats.plotconditions])
    conditiondata = np.ma.array(fishstats.conditiondata_time[:,fishstats.plotconditions,:])
    
    if nocontrol == False:
        controlmask = np.ma.getmask(fishstats.controldata_clean[:,fishstats.plotcontrols])
        controldata = np.ma.array(fishstats.controldata_time[:,fishstats.plotcontrols,:])
    
        data = np.hstack((controldata,conditiondata))
        mask = np.hstack((controlmask,conditionmask))
    else:
        data = conditiondata
        mask = conditionmask 

        

    for j in range(data.shape[-1]):
        data[:,:,j] = np.ma.masked_array(data[:,:,j],mask)
   
    endpoint = np.argmin(abs(time - fishstats.endtime ))
    
    # plot time series of cumulative data
    plotwindow = tk.Toplevel()
    fig1 = plt.figure(figsize=(10,round(10/aspect)),tight_layout=True)
    canvas1 = FigureCanvasTkAgg(fig1,master=plotwindow)
    canvas1.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1.0)
    ax1 = fig1.add_subplot(111)
    
  
       
    ax1.text(0.75,0.05,"Temporal resolution: %i s" % np.mean(np.diff(time)),transform=ax1.transAxes,fontsize=fishstats.fontsize)

    for i in range(0,np.shape(data)[1]):
        series = data[:,i,:]

        if fig1.get_size_inches()[0]*fig1.dpi/np.shape(series)[-1] < 10:  # threshold for turning markers on/off to make plots more legible.
            marker =  ''
        else:
            marker= markers[i%len(markers)]
            
        ax1.plot(time-time[0],np.cumsum(np.mean(series,axis=0)),label=labels[i],color=colors[i%len(colors)],marker=marker)
    ax1.legend()
    xlabel1 = ax1.set_xlabel("Time (s)",fontsize=fishstats.fontsize)
    ylabel1 = ax1.set_ylabel("Cumulative distance swum (cm)",fontsize=fishstats.fontsize)
    ax1.set_xlim([0,time[endpoint]-time[0]])
    ax1.set_ylim(bottom=0)
   
    
    plotwindow2 = tk.Toplevel()
    fig2 = plt.figure(figsize=(10,round(10/aspect)),tight_layout=True)
    canvas2 = FigureCanvasTkAgg(fig2,master=plotwindow2)
    canvas2.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1.0)
    ax2 = fig2.add_subplot(111)
    
    ax2.text(0.75,0.05,"Temporal resolution: %i s" % np.mean(np.diff(time)),transform=ax2.transAxes,fontsize=fishstats.fontsize)    

    for i in range(0,np.shape(data)[1]):
        series = data[:,i,:]
        if fig1.get_size_inches()[0]*fig1.dpi/np.shape(series)[-1] < 10:  # threshold for turning markers on/off to make plots more legible.
            marker =  ''
        else:
            marker= markers[i%len(markers)]
        ax2.plot(time-time[0],np.mean(series,axis=0),label=labels[i],color=colors[i%len(colors)],marker=marker)
    ax2.legend()
    xlabel2 = ax2.set_xlabel("Time (s)",fontsize=fishstats.fontsize)
    ylabel2 = ax2.set_ylabel("Distance swum (cm)",fontsize=fishstats.fontsize)
    ax2.set_xlim([0,time[endpoint]-time[0]])
    ax2.set_ylim(bottom=0)

   # plt.show()
    canvas1.draw()
    canvas2.draw()

    fig1.savefig(fishstats.outputpath[:-1] +"_fishstats_cumulative-vs-time_"+ str(fishstats.starttime).zfill(4) + "-" + str(fishstats.endtime).zfill(4)+poolstring+".png",
                 dpi=500,bbox_extra_artists=(xlabel1,ylabel1), bbox_inches='tight')    
    fig2.savefig(fishstats.outputpath[:-1] +"_fishstats-timeseries_"+ str(fishstats.starttime).zfill(4) + "-" + str(fishstats.endtime).zfill(4)+poolstring+".png",
                 dpi=500,bbox_extra_artists=(xlabel2,ylabel2), bbox_inches='tight')     



    
def plotContours(fishstats):
    for i, data in enumerate(fishstats.platedata):
        figname = fishstats.outputpath[:-1] + "_fishstats_contours_" + str(fishstats.starttime).zfill(4) + "-" + str(fishstats.endtime).zfill(4) + "_plate" + str(i+1) 
    
    
        plotwindow = tk.Toplevel()
        fig3 = plt.figure(figsize=(12,8))
        canvas3 = FigureCanvasTkAgg(fig3,master=plotwindow)
        canvas3.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1.0)
        ax3 = fig3.add_subplot(111)
       

        mat = ax3.matshow(data)
        for y in np.arange(-0.5, np.shape(data)[0]+1-0.5):
            ax3.axhline(y, lw=2, color='k', zorder=5)
            
        for x in np.arange(-0.5, np.shape(data)[1]+1-0.5):
            ax3.axvline(x, lw=2, color='k', zorder=5)       
        
        divider = make_axes_locatable(ax3)
        cax = divider.append_axes("right", size="5%", pad=0.15)
        bar = fig3.colorbar(mat,cax=cax)
        ax3.set_xticks(np.arange(0,fishstats.cols+1))
        ax3.set_xticklabels([str(i+1) for i in np.arange(0,fishstats.cols)])
        ax3.set_yticks(np.arange(fishstats.rows-1,-1,-1))
        ax3.set_yticklabels([str(i+1) for i in np.arange(0,fishstats.rows)])
        
        ax3.set_xlim(-0.5,11.5)
        ax3.set_ylim(-0.5, 7.5)
        bar.set_label("Cumulative distance swum (cm)")
        ax3.set_title("Time Period: (%i s - %i s)" % (fishstats.starttime, fishstats.endtime))
        print("Contour plot of cumulative distance for plate %i saved to %s" % (i+1, figname+".png"))
        
        canvas3.draw()
       # plt.show()
        fig3.savefig(figname+".png",dpi=500)        

###############################################################################
# EVERYTHING BELOW THIS LINE IS CURRENTLY UNUSED ------------------------------
###############################################################################

def vomitData(data,step):
    for fish in data[step]:
        fish_num = int((int(re.findall(r'\d+', fish)[0]))/conds)
        # use modulo division to find the condition number
        cond = (int(re.findall(r'\d+', fish)[0])-1)%conds + 1 
        print(">%s - Condition %i, Fish %i (%i-%i) inadist: %2.3f smldist: %2.3f lardist: %2.3f totaldist: %2.3f" % 
                                                     (fish, cond,fish_num,
                                                     data[step][fish]['start'], 
                                                     data[step][fish]['end'],
                                                     data[step][fish]['inadist'],
                                                     data[step][fish]['smldist'],
                                                     data[step][fish]['lardist'],
                                                     data[step][fish]['inadist']+data[step][fish]['smldist']+data[step][fish]['lardist']))





