This program [should] analyze and consolidate data on fish swimming; it is configured to output text files (.dat extension) for each cycle containing the total distance of each fish as well as basic statistics. 

Inputs (*= optional):
	The script reads a few basic user inputs from a file:
		path_to_datafile: the datafile (assumed to be tab-delimited) where the data are stored; the program will by default look in the working directory for this file
		*path_to_controlfile: the controls datafile (assumed to be tab-delimited) where the control data are stored; the program will by default look in the working directory for this file; if it is not found, it will instead read controls from the datafile. The program will (should?) apply the same data grouping to both the data and control files. I think.
		fish_per_cond: the number of fish in each condition
		conds: the number of conditions
		starttime: the intial time of relevant time interval (in seconds)
		endtime: the end time of relevant time interval (in seconds)
		*datagroups: groupings of different conditions (numbers correspond to columns on plate - 0 is first, 1 is second, etc.) format is group1cond1,group1cond2... group1condn ; group2cond1, group2cond2... group2condn 
		labels: labels for each group to be used for plotting. Format is label1, label2, ... labeln

	The script will look for "fishstats_inputs.txt" in the working directory; optionally, a different input file can also be passed directly through the command line. 

Outputs:
-One .dat file for each cycle, containing data for each fish in each condition as well as basic statistics both with and without zero values included
-One .png file containing a bar plot with error bars for each condition/group
-One .png file containing a time plot of cumulative average distance swum by each fish group
-One .png file containing a time series of instantaneous distance swum over each time step within the time period

Usage:
python fishstatsv2.py [input file] [-shush]

Each fish will be printed to stdout when the program executes unless the -shush option is passed.