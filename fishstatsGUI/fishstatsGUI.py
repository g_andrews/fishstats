# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 08:32:54 2022

@author: Geoffrey Andrews

Graphical user interface for FishStats, incorporating all capabiilities of 
previous FishStats versions (v.0 - v.5), with the added capability of turning 
analysis modes on or off.

"""

import tkinter as tk
import tkinter.filedialog
from modules import inspectFiles
from modules import readDataFiles
from modules import doStats
from modules import plotData
from modules import plotDataSimple
from modules import cleanData
from modules import writeData
from modules import writeDataSimple
from modules import plotTimeSeries
from modules import plotContours
from modules import poolData
import numpy as np


class FishStats:
    def __init__(self, cols,rows):
        self.cols = cols
        self.rows = rows
        self.directory = './'
        self.version = "6.1.5"
        print("Activating FishStats v%s:" % self.version)

    datafile_paths = ()
    controlfile_paths = ()
    outputpath = './'
    conditions = []
    conditionnames = []
    controls = []
    controlnames = []
    conditiondata = np.zeros(0)
    controldata = np.zeros(0)
    timeseries=np.zeros(0)
    conditiondata_time = np.zeros(0)
    controldata_time = np.zeros(0)
    plotconditions = []
    plotcontrols = []
    controlstats_raw = {}
    controlstats_clean = {}
    conditionstats_raw = {}
    conditionstats_clean = {}
    conditiondata_clean = np.zeros(0)
    controldata_clean = np.zeros(0)
    starttime = 0
    endtime = 10
    fontsize = 10
    platedata=[]

def inspectDataFiles(fishstats):
    new_datafile_paths = tk.filedialog.askopenfilename(multiple=True, title="Select data files")
    fishstats.datafile_paths = fishstats.datafile_paths + new_datafile_paths
    for i,file in enumerate(new_datafile_paths):
        datafiles_disp.insert(tk.END, file + '\n')
    conditions = inspectFiles(fishstats,new_datafile_paths)
    for condition in conditions:
        conditionsbox.insert(tk.END,condition + '\n')
        if controlsource.get() == 0:
            controlsbox.insert(tk.END,condition + '\n')   
        
def inspectControlFiles(fishstats):
    controlsbox.delete(0,tk.END)
    new_controlfile_paths = tk.filedialog.askopenfilename(multiple=True, title="Select control file ")
    fishstats.controlfile_paths=fishstats.controlfile_paths + new_controlfile_paths
    for i,file in enumerate(new_controlfile_paths):
        controlfiles_disp.insert(tk.END, file + '\n')
    controls = inspectFiles(fishstats,new_controlfile_paths,'control')
  
    if (controlsource.get() == 0)==False:
        #controlsbox.insert(tk.END,conditionsbox.get(0).strip() + '\n')
   # else:
        for control in controls:
            controlsbox.insert(tk.END,control + '\n') 
        
def openDataFiles(fishstats):       
    [conditions,conditiondata] = readDataFiles(fishstats)
    fishstats.conditiondata = conditiondata

      
def openControlFile(fishstats):
    [controls,controldata] = readDataFiles(fishstats,'control')
    fishstats.controldata = controldata
    
    
def controlFromAdjacent():
    controlfilebutton['state'] = tk.DISABLED
    controlfiles_disp['state'] = tk.DISABLED      
    controlsbox.delete(0,tk.END)

def disableControlFile():
    controlfilebutton['state'] = tk.DISABLED
    controlfiles_disp['state'] = tk.DISABLED      
    controlsbox.delete(0,tk.END)   

def getStartTime(fishstats):
    fishstats.starttime = int(starttime_box.get())
    print("Analysis period start: %s s" % fishstats.starttime)
    
def getEndTime(fishstats):
    fishstats.endtime = int(endtime_box.get())
    print("Analysis period end: %s s" % fishstats.endtime)
    
def setOutput(fishstats):
    filepath = tk.filedialog.askdirectory()
    output_disp.insert(tk.END,filepath + '\n')
    fishstats.directory = filepath + "/"
    
def checkInputs(fishstats):
    error = False
    try:
        int(starttime_box.get())
    except:
        print("No start time specified! Enter a start time and run again.")
        error = True
    try:
        int(endtime_box.get())
    except:
        print("No end time specified! Enter an end time and run again.") 
        error = True
    if starttime_box.get() >= endtime_box.get():
        print("End time must be greater than start time.")
        error = True
    if len(conditionsbox.curselection()) == 0:
        print("No conditions selected! Please select conditions for analysis.")
        error = True
    if len(controlsbox.curselection()) == 0 and usecontrolfile.get() == True :
        print("No controls selected! Please select control conditions.")
        error = True
    if len(output_disp.get(1.0,tk.END)) == 1:
        print("No destination for output files selected! Please select an output destination.")
        error = True
    if len(controlsbox.curselection()) > 1 and pool.get==True:
        print("Multiple control conditions are selected but pooling is not enabled. This will not produce the results you want!")
        error = True
    return error
        
def runAnalysis(fishstats):
    reset(fishstats)  
    errors = checkInputs(fishstats)
    if errors == True:
        return
    getStartTime(fishstats)
    getEndTime(fishstats)
    
    fishstats.fontsize = int(fontsize_box.get())
    
    print("><> ><> ><> BEGINNING ANALYSIS <>< <>< <><")
    [conditions,conditiondata] = readDataFiles(fishstats)
    fishstats.conditiondata=conditiondata

 #   for i in conditionsbox.curselection():
    for i,name in enumerate(conditionsbox.get(0,tk.END)):
        fishstats.conditionnames.append(conditionsbox.get(i).strip() + " (U)")
        fishstats.conditionnames.append(conditionsbox.get(i).strip() + " (I)") 
        fishstats.conditions.append(2*i)
        fishstats.conditions.append(2*i+1)
        
    fishstats.outputpath = fishstats.directory + '-'.join([conditionsbox.get(i).split('\n')[0] for i in conditionsbox.curselection()])
    print("The following drugs have been selected:")
    if controlsource.get() == 2:
        print("No control source is being used; stats are being compared between adjacent conditions")
        for i in conditionsbox.curselection():
            print("   " + conditionsbox.get(i).strip())  
            fishstats.plotcontrols.append(2*i)
            fishstats.plotconditions.append(2*i+1)        
    else:
        for i in conditionsbox.curselection():
            print("   " + conditionsbox.get(i).strip())   
            if plotuninjured.get() == True:
                fishstats.plotconditions.append(2*i)
            if plotinjured.get() == True:
                fishstats.plotconditions.append(2*i+1)
 
    if controlsource.get() ==0:
        print("The following conditions are being used as controls:")
        [controls,controldata] = [conditions, conditiondata]
        fishstats.controldata = controldata
        fishstats.controldata_time = fishstats.conditiondata_time
        for i,name in enumerate(controlsbox.get(0,tk.END)):
            fishstats.controlnames.append(controlsbox.get(i).strip() + " (U)")
            fishstats.controlnames.append(controlsbox.get(i).strip() + " (I)")        
            fishstats.controls.append(2*i)
            fishstats.controls.append(2*i+1)  
        for i in controlsbox.curselection():
            fishstats.plotcontrols.append(2*i)
            if plotinjured.get() == True:
                fishstats.plotcontrols.append(2*i+1)
                
    #case where controls come from external file                
    if controlsource.get() == 1 :
        print("The following conditions are being used as controls:")
        [controls,controldata] = readDataFiles(fishstats,'control')
        fishstats.controldata=controldata
        for i,name in enumerate(controlsbox.get(0,tk.END)):
            fishstats.controlnames.append(controlsbox.get(i).strip() + " (U)")
            fishstats.controlnames.append(controlsbox.get(i).strip() + " (I)")        
            fishstats.controls.append(2*i)
            fishstats.controls.append(2*i+1)  
        for i in controlsbox.curselection():
      #      fishstats.controlnames.append(controlsbox.get(i).strip() + " (U)")
     #       fishstats.controlnames.append(controlsbox.get(i).strip() + " (I)")        
    #        fishstats.controls.append(2*i)
   #         fishstats.controls.append(2*i+1)  

            fishstats.plotcontrols.append(2*i)
            if plotinjured.get() == True:
                fishstats.plotcontrols.append(2*i+1)
                
    #case where each column uses the adjacent column as a control (i.e. injured/uninjured)
    elif controlsource.get() == 2:
        fishstats.controlfile_paths = fishstats.datafile_paths

        [controls,controldata] = readDataFiles(fishstats,'control')
        fishstats.controldata = controldata
        fishstats.controls = fishstats.conditions
        fishstats.controlnames = fishstats.conditionnames
 
    # case where a selected data condition is used as a control (CURRENTLY UNUSED)
    elif controlsource.get() == 400:
        print("The following conditions are being used as controls:")
        for i,name in enumerate(controlsbox.curselection()):
            print(name)
            fishstats.controlnames.append(name.strip() + " (U)")
            fishstats.controlnames.append(name.strip() + " (I)")    
            fishstats.controls.append(2*i)
            fishstats.controls.append(2*i +1) 
            fishstats.plotcontrols.append(0)
            if plotinjured.get() == True:
                fishstats.plotcontrols.append(1)
            fishstats.controldata = fishstats.conditiondata[:,0:2]
            fishstats.controldata_time = fishstats.conditiondata_time[:,0:2,:]
        
    # case for no controls whatsoever
    elif controlsource.get() == 3:
        print("No control source is being used; available conditions will be plotted with no statistical comparison")

    
    # Pool all like drugs and like controls    
    if pool.get() == True:
        poolData(fishstats)
        fishstats.outputpath = fishstats.directory + '-'.join(set([conditionsbox.get(i).split('\n')[0] for i in conditionsbox.curselection()]))+'-'
        
    # Do basic statistics  
    if controlsource.get() == 2:
        fishstats.conditionstats_raw = doStats(fishstats.conditiondata[:,fishstats.conditions],
                control=fishstats.controldata[:,fishstats.controls],multi=True) 
        fishstats.controlstats_raw = doStats(fishstats.controldata[:,fishstats.controls],multi=True)
    elif controlsource.get() == 3:
        fishstats.conditionstats_raw = doStats(fishstats.conditiondata[:,fishstats.conditions],nocontrol=True) 
        
    else:
        fishstats.conditionstats_raw = doStats(fishstats.conditiondata[:,fishstats.conditions],
                control=fishstats.controldata[:,fishstats.controls]) 
        fishstats.controlstats_raw = doStats(fishstats.controldata[:,fishstats.controls])        
        
    # Apply user inputs for data cleaning
    if exclude2sd.get() == True and exclude0.get() == True:
        cleanData(fishstats,'remove2sd','remove0')
    elif exclude2sd.get() == True:
        cleanData(fishstats,'remove2sd')
    elif exclude0.get() == True:
        cleanData(fishstats,'remove0')
    else:
        fishstats.conditiondata_clean = fishstats.conditiondata
        fishstats.controldata_clean = fishstats.controldata

    # Rerun stats
    if controlsource.get() == 2:
        fishstats.conditionstats_clean = doStats(fishstats.conditiondata_clean[:,fishstats.conditions],
                control=fishstats.controldata_clean[:,fishstats.controls],multi=True)
        fishstats.controlstats_clean = doStats(fishstats.controldata_clean[:,fishstats.controls],multi=True)
    elif controlsource.get() == 3:
        fishstats.conditionstats_clean = doStats(fishstats.conditiondata_clean[:,fishstats.conditions],
                nocontrol=True)
    else:
        fishstats.conditionstats_clean = doStats(fishstats.conditiondata_clean[:,fishstats.conditions],
                control=fishstats.controldata_clean[:,fishstats.controls])
        fishstats.controlstats_clean = doStats(fishstats.controldata_clean[:,fishstats.controls])

    # Create plots
    
        
    if doColumns.get() == True:
        
        if controlsource.get() == 2:
            if plotfish.get() == True:
                plotData(fishstats,'plotfish','multi')
            else:
                plotData(fishstats,'multi')
        elif controlsource.get() == 3:
            for j in fishstats.plotconditions:
                if np.all(fishstats.conditiondata[:,j]==0):
                    fishstats.plotconditions.remove(j)           
            if plotfish.get() == True:
                plotDataSimple(fishstats,'plotfish')
            else:
                plotDataSimple(fishstats)            
        else:
            if plotfish.get() == True:
                plotData(fishstats,'plotfish')
            else:
                plotData(fishstats)                           
        
    if doTimeSeries.get() == True:
        TS_aspect = float(ts_width_box.get())*5./3.
        if controlsource.get() == 3:
            if plotinjured.get() == True and plotuninjured.get() == False:
                plotTimeSeries(fishstats,TS_aspect,'injured','nocontrol')
            elif plotinjured.get() == False and plotuninjured.get() == True:
                plotTimeSeries(fishstats,TS_aspect,'uninjured','nocontrol')
            else:
                plotTimeSeries(fishstats,TS_aspect,'injured','uninjured','nocontrol')            
        else:
            if plotinjured.get() == True and plotuninjured.get() == False:
                plotTimeSeries(fishstats,TS_aspect,'injured')
            elif plotinjured.get() == False and plotuninjured.get() == True:
                plotTimeSeries(fishstats,TS_aspect,'uninjured')
            else:
                plotTimeSeries(fishstats,TS_aspect,'injured','uninjured')
            
    if doContours.get() == True:
        print("Generating contour plots...")
        plotContours(fishstats)

    # Write data to output file    
    if controlsource.get()==3:
        writeDataSimple(fishstats)
    else:
        writeData(fishstats)
    
    print("Analysis complete.")
    
def reset(fishstats):
    fishstats.conditions = []
    fishstats.conditionnames = []
    fishstats.controls = []
    fishstats.controlnames = []
    fishstats.plotconditions = []
    fishstats.plotcontrols = []
    fishstats.controlstats_raw = {}
    fishstats.controlstats_clean = {}
    fishstats.conditionstats_raw = {}
    fishstats.conditionstats_clean = {}
    fishstats.conditiondata_clean = np.zeros(0)
    fishstats.controldata_clean = np.zeros(0)
    closeWindows()
    
    
def clear(fishstats):
    fishstats.datafile_paths = ()
    fishstats.controlfile_paths = ()
    reset(fishstats)
    conditionsbox.delete(0,tk.END)
    controlsbox.delete(0,tk.END)
    controlfiles_disp.delete(1.0,tk.END)
    datafiles_disp.delete(1.0,tk.END)
    closeWindows()
  #  output_disp.delete(1.0,tk.END)
  
def closeWindows():
    for widget in master.winfo_children():
        if isinstance(widget, tk.Toplevel):
            widget.destroy()

def controlFromExternal():
    controlfilebutton['state'] = tk.NORMAL
    controlfiles_disp['state'] = tk.NORMAL
    controlsbox.delete(0,tk.END)
    
def controlFromDataFile():
    controlfilebutton['state'] = tk.DISABLED
    controlfiles_disp['state'] = tk.DISABLED  

    controlsbox.insert(tk.END,conditionsbox.get(0).strip() + '\n')

color1 = "#f7edff"

master = tk.Tk()
master.configure(bg="#ffffff")
master.title("><> Fishstats v6.1 <><")
master.lift()

fishstats = FishStats(12,8)
controlsource = tk.IntVar(value=0)
usecontrolfile = tk.BooleanVar()
usenocontrol = tk.BooleanVar()
plotinjured = tk.BooleanVar(value=True)
plotuninjured = tk.BooleanVar(value=True)
plotfish = tk.BooleanVar(value=False)
doColumns = tk.BooleanVar()
doTimeSeries = tk.BooleanVar()
doContours = tk.BooleanVar()
exclude2sd = tk.BooleanVar(value=True)
exclude0 = tk.BooleanVar(value=False)
pool = tk.BooleanVar(value=False)
conditions=tk.StringVar(value='')
controls=tk.StringVar(value='')
selected_drugs = tk.StringVar(value='')
selected_controls = tk.StringVar(value='')
fontsize = tk.IntVar(value=10)
TS_aspect = tk.DoubleVar(value=1.0)



datainfo = tk.Frame(master,
          highlightbackground="black", 
          highlightthickness=0,
          bg = color1)
datalabel = tk.Label(datainfo,text="Data file Information",
          bg = color1)
datalabel.pack()
datafilebutton = tk.Button(datainfo,
        text="Select data file",
        width=25,
        height=1,
        command = lambda: inspectDataFiles(fishstats))
datafilebutton.pack()
datafiles_disp = tk.Text(datainfo,height=5,width=20,wrap=tk.NONE)
datafiles_disp.pack()

conditionsboxlabel = tk.Label(datainfo,text="Drug conditions found in data files:",
          bg = color1)
conditionsboxlabel.pack()
conditionsbox=tk.Listbox(datainfo,
         width=25,
         height=5,
         listvariable=conditions,
         selectmode='extended',
         exportselection=0)
conditionsbox.pack()

datainfo.grid(row=0,column=0,
              sticky=tk.E+tk.W+tk.N+tk.S,
              padx=5,
              pady=5,
              ipady=3)

controlinfo = tk.Frame(master,
          highlightbackground="black", 
          highlightthickness=0,
          bg = color1)
controllabel = tk.Label(controlinfo,text="Control file Information",
          bg = color1)
controllabel.pack()
controlfromdata = tk.Radiobutton(controlinfo,
         text="Use conditions in data file",
         variable=controlsource,
         tristatevalue=0,
         command=lambda: controlFromDataFile(),
         value=0,
         bg = color1)
controlfromdata.pack(anchor='w')
controlfromfile = tk.Radiobutton(controlinfo,
         text="Get control data from external file",
         variable=controlsource,
         command=lambda: controlFromExternal(),
         tristatevalue=1,
         value=1,
         bg = color1)
controlfromfile.pack(anchor='w')
controladjacent = tk.Radiobutton(controlinfo,
         text="Use adjacent columns in data file",
         variable=controlsource,
         tristatevalue=2,
         value=2,
         command=lambda: controlFromAdjacent(),
         bg = color1)
controladjacent.pack(anchor='w')
controlnone = tk.Radiobutton(controlinfo,
         text="No control data",
         variable=controlsource,
         tristatevalue=3,
         value=3,
         command=disableControlFile,
         bg = color1)
controlnone.pack(anchor='w')
controlfilebutton = tk.Button(controlinfo,
        text="Select control file",
        width=25,
        height=1,
        command = lambda: inspectControlFiles(fishstats),
        state=tk.DISABLED)
controlfilebutton.pack()
controlfiles_disp = tk.Text(controlinfo,height=1,width=20,wrap=tk.NONE,state=tk.NORMAL)
controlfiles_disp.pack()
controlsboxlabel = tk.Label(controlinfo,text="Control conditions:",
                            bg = color1)
controlsboxlabel.pack()
controlsbox=tk.Listbox(controlinfo,
         width=20,
         height=3,
         listvariable=controls,
         selectmode='extended',
         exportselection=0)
controlsbox.pack()


controlinfo.grid(row=0,column=1,
              sticky=tk.E+tk.W+tk.N+tk.S,
              padx=5,
              pady=5,
              ipady=3)

analysisinfo=tk.Frame(master,
          highlightbackground="black", 
          highlightthickness=0,
          bg = color1)
analysislabel = tk.Label(analysisinfo,text="Analysis Options",
          bg = color1)
analysislabel.pack()

timeinputs = tk.Frame(analysisinfo)

starttime_label = tk.Label(timeinputs, text = "Start time:",
          bg = color1)
starttime_label.pack(side=tk.LEFT,anchor='w')
starttime_box = tk.Entry(timeinputs,
                         width=5)
starttime_box.pack(side=tk.LEFT,anchor='w')

endtime_label = tk.Label(timeinputs, text = "End time:",
          bg = color1)
endtime_label.pack(side=tk.LEFT,anchor='e')
endtime_box = tk.Entry(timeinputs,
                       width=5)
endtime_box.pack(side=tk.LEFT,anchor='e')

timeinputs.pack()

remove2sd_button = tk.Checkbutton(analysisinfo,
         text=u"Exclude values greater than  2\u03c3 from mean",
         variable=exclude2sd,
         onvalue=True,
         offvalue=False,
         bg = color1)
remove2sd_button.pack(side=tk.BOTTOM,anchor='w')

exclude0_button = tk.Checkbutton(analysisinfo,
         text="Exclude 0 values",
         variable=exclude0,
         onvalue=True,
         offvalue=False,
         bg = color1)
exclude0_button.pack(side=tk.BOTTOM,anchor='w')

pool_button = tk.Checkbutton(analysisinfo,
         text="Pool datasets with shared names",
         variable=pool,
         onvalue=True,
         offvalue=False,
         bg = color1)
pool_button.pack(side=tk.BOTTOM,anchor='w')

analysisinfo.grid(row=5,column=0,
              sticky=tk.E+tk.W+tk.N+tk.S,
              padx=5,
              pady=5)

plotinfo=tk.Frame(master,
          highlightbackground="black", 
          highlightthickness=0,

          bg = color1)
plotlabel = tk.Label(plotinfo,text="Plotting Information",
          bg = color1)
plotlabel.pack()

injurystate_buttons = tk.Frame(plotinfo)
plotuninjured_button = tk.Checkbutton(injurystate_buttons,
         text="Plot Uninjured",
         variable=plotuninjured,
         onvalue=True,
         offvalue=False,
         bg = color1)
plotuninjured_button.pack(side=tk.RIGHT)
plotinjured_button = tk.Checkbutton(injurystate_buttons,
         text="Plot Injured",
         variable=plotinjured,
         onvalue=True,
         offvalue=False,
         bg = color1)
plotinjured_button.pack(side=tk.LEFT)

plotfish_button = tk.Checkbutton(injurystate_buttons,
         text="Plot Individual Fish",
         variable=plotfish,
         onvalue=True,
         offvalue=False,
         bg = color1)
plotfish_button.pack(side=tk.LEFT)

injurystate_buttons.pack(side=tk.TOP)
plottype_buttons = tk.Frame(plotinfo)
doColumns_button = tk.Checkbutton(plottype_buttons,
         text="Column Chart",
         variable=doColumns,
         onvalue=True,
         offvalue=False,
         bg = color1)
doColumns_button.pack(side=tk.LEFT)
doTimeSeries_button = tk.Checkbutton(plottype_buttons,
         text="Time Series",
         variable=doTimeSeries,
         onvalue=True,
         offvalue=False,
         bg = color1)
doTimeSeries_button.pack(side=tk.LEFT)
doContours_button = tk.Checkbutton(plottype_buttons,
         text="Plate Contour Plot",
         variable=doContours,
         onvalue=True,
         offvalue=False,
         bg = color1)
doContours_button.pack(side=tk.LEFT)
plottype_buttons.pack()

fontsize_label = tk.Label(plotinfo, text = "Font size for plots:",
          bg = color1)
fontsize_label.pack(side=tk.LEFT,anchor='w')
fontsize_box = tk.Entry(plotinfo,
                         width=5)
fontsize_box.insert(0,'10')
fontsize_box.pack(side=tk.LEFT,anchor='w')

ts_width_label = tk.Label(plotinfo, text = " Width of time series plot:",
          bg = color1)
ts_width_label.pack(side=tk.LEFT,anchor='w')

ts_width_box = tk.Entry(plotinfo,
                         width=3)
ts_width_box.insert(0,'1.0')
ts_width_box.pack(side=tk.LEFT,anchor='w',ipadx=5)




plotinfo.grid(row=4,column=0,
              sticky=tk.E+tk.W+tk.N+tk.S,
              padx=5,
              pady=5)

outputinfo=tk.Frame(master,
          highlightbackground="black", 
          highlightthickness=0,
          bg = color1)
outputlabel = tk.Label(outputinfo,text="Output Information",
          bg = color1)
outputlabel.pack()
selectoutput = tk.Button(outputinfo, text="Select output destination", command = lambda: setOutput(fishstats))
selectoutput.pack()
output_disp = tk.Text(outputinfo,height=1,width=20,wrap=tk.NONE,state=tk.NORMAL)
output_disp.pack()
outputinfo.grid(row=4,column=1,
              sticky=tk.E+tk.W+tk.N+tk.S,
              padx=5,
              pady=5)

buttons = tk.Frame(master)
startbutton = tk.Button(buttons,
        text="Start analysis",
        width=20,
        height=1,
        command = lambda: runAnalysis(fishstats))
startbutton.pack()

resetbutton = tk.Button(buttons,
        text="Reset",
        width=20,
        height=1,
        command = lambda: clear(fishstats))
resetbutton.pack()
buttons.grid(row=5,column=1)

master.mainloop()
