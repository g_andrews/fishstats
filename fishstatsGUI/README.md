Welcome to the shiny new version of FishStats, now featuring a graphical user interface!

FishStats 6.0 builds upon all of the existing FishStats functionality you've come to know and love, 
but now with a much more user-friendly, easy-to-use interface. Huzzah!

SETUP:   Clone or download a the FishStats repository from 

		 https://gitlab.com/g_andrews/fishstats/-/tree/master/fishstatsGUI

         It is recommended to set up a directory/folder on your computer that is just for the code; 
         this is crucial if you are using git to clone and update FishStats but it's good practice 
         either way. You can set up other directories/folders for your data, output, etc.

         Example:
         /user
         -> /FishStats
         ----> /code
         ----> /data
         ----> /outputs

TO RUN: 
Windows: FishStats can either be run from a Python IDE like Spyder, or freestanding from the command 
		 line.To run from the command line, simply open up a command prompt (which you can do from 
		 the Start Menu), navigate to the directory where you have saved the code, and run:

         python fishstatsGUI.py

         Assuming that you have a working version of Python 3, this should immediately launch the 
         graphical interface.

         To run from an IDE, open the IDE, load the code, and hit "run" to bring up the graphical
         interface.

OS X:    Currently, FishStats can only be run from the terminal on OS X. Fear not, this is very 
 		 simple. Simply open a Terminal window and navigate to the directory where your code is 
 		 stored (cd newdir will change to the directory newdir; cd .. will move back a directory;
 		 pwd will show your current directory; and ls will show all files and directories in the 
 		 current directory). Then, just run python fishstatsGUI.py.

