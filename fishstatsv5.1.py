# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 17:38:40 2020

@author: Geoffrey

UPDATED TO USE FORMATTING FROM NEW VMR MACHINE
And also to read data over a specified time period by integrating across each time step
plots time series of fish movement

UPDATED 9-21-21 to fix stats issues and minor plotting bugs

"""

import numpy as np
import os
import re
from scipy import stats
import sys
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import pylab as pl
from mpl_toolkits.axes_grid1 import make_axes_locatable
from statsmodels.graphics.gofplots import qqplot
import tkinter as tk
import tkinter.filedialog

mpl.rcParams['mathtext.default'] = 'regular'

def main():
    allcontrols = np.zeros((int(conds*fish_per_cond/len(datagroups)),2))
    alllabels=[]
    numcontrols = 0
    for path_to_datafile in files:
        print("Reading data from " + path_to_datafile)
        platelabels = []
             
        
        [masterdata,steps] = readDataFile(path_to_datafile)
        
        timeseries = []       
        period_data = np.zeros((fish_per_cond,conds,len(steps)))
    
        for i in np.arange(0, len(steps)):
           # if int(steps[i]['start']) >= starttime and int(steps[i]['end']) <= endtime+1:        
            step = i
            # Read in data    
            period_data[:,:,i] = parseData(masterdata,step)                  
            timeseries.append(steps[i]['start'])
               
            if vomit == True:
                vomitData(masterdata,step) 
        timeseries = np.asarray(timeseries)
    
        # form groups of data
        grouped_data = np.zeros((fish_per_cond* max(map(len,datagroups)),len(steps),len(datagroups)))
           
        for i,group in enumerate(datagroups):
           # print("data group: %i" % i)
          #  print(group)
            if i < len(controlgroups):
                controlgroup = controlgroups[i]
            else:
                controlgroup = controlgroups[0]
            size = len(group)*fish_per_cond
            data = period_data[:,group,:].reshape((size,len(steps)),order='C').T
            grouped_data[:,:,i] = data.T

            
        # add up data to find cumulative value       
        cumulative_data = np.sum(grouped_data,axis=1)
        print(path_to_datafile)

        for i,drugname in enumerate(path_to_datafile.split('/')[-1].split('-')[1:4]):
      

            if drugname=='DMSO':
                allcontrols = allcontrols + cumulative_data[:,i:i+2]
                numcontrols = numcontrols + 1
                print("Adding DMSO to control data...")
                platelabels.append(drugname + " (U)")
                platelabels.append(drugname + " (I)")                 
            else:
                print("Adding %s to drug data..." % drugname)
                if 'alldrugs' in locals():
                    alldrugs = np.hstack((alldrugs, cumulative_data[:,2*i:2*i+2]))
                else:
                    alldrugs = cumulative_data[:,2*i:2*i+2]
                alllabels.append(drugname)
                alllabels.append(drugname)                
                platelabels.append(drugname + " (U)")
                platelabels.append(drugname + " (I)") 

    alllabels.insert(0,'DMSO (U)')
    alllabels.insert(1,'DMSO (I)')
    allcontrols = allcontrols/numcontrols  

    # statistics and black magic
    stats_raw = doStats(alldrugs,control=allcontrols)
    controlstats_raw = doStats(allcontrols)

    # clean data
    # Remove anything greater than 2 standard deviations away
    print(r"Removing data greater than 2 stdev away from mean")
    good_data = np.ma.array(alldrugs)
    for i in np.arange(0,np.shape(alldrugs)[-1]):
     #   print("  Group %i (Category: '%s')" % (i+1, labels[i]))
        print("    Mean: %3.2f; stdev= %3.2f" % (stats_raw['mean_vals'][i], stats_raw['stdev'][i]))
        good_data[:,i] = np.ma.masked_outside(alldrugs[:,i], 
                                     stats_raw['mean_vals'][i] - stats_raw['stdev'][i]*2,
                                     stats_raw['mean_vals'][i] + stats_raw['stdev'][i]*2)
        print("    %i of %i values are within the range %3.2f - %3.2f" % (good_data[:,i].count(), len(alldrugs[:,i]),stats_raw['mean_vals'][i] - stats_raw['stdev'][i]*2,
                                     stats_raw['mean_vals'][i] + stats_raw['stdev'][i]*2))
        print("    There are %i zero values." % (len(alldrugs[:,i])-np.count_nonzero(alldrugs[:,i])))
  
        # Remove 0 values
        good_data[:,i] = np.ma.masked_equal(good_data[:,i],0)

    stats_clean = doStats(good_data,control=allcontrols)#,normtest='False')
    controlstats_clean = doStats(allcontrols)

#
#    
##    # Write data to file
##    writeData(good_data,step,starttime,endtime,stats_raw,stats_clean)
##    #print(stats_clean['mean_vals'])
##  #  print(stats_clean['Tvals'])
##   # print(stats_raw['mean_vals'])
##    print(stats_raw['Tvals']-stats_clean['Tvals'])
##
#    # Plot cumulative data
    plotData(good_data,stats_clean,step,starttime,endtime, control=allcontrols,controlstats=controlstats_clean,labels=alllabels)
##    
##    # Plot time series
##    mask = np.ma.getmask(good_data)
##    good_time_data = np.ma.array(grouped_data)
##
##    for j in range(grouped_data.shape[1]):
##        good_time_data[:,j,:] = np.ma.masked_array(grouped_data[:,j,:],mask)
##    plotTimeSeries(timeseries, good_time_data, labels)
##
##    plotContours(np.sum(period_data,axis=2),starttime,endtime) 
##        
##    
    
def openInputFile(path):   
    try:    
        inputfile = open(path)
    except:
        sys.exit("Input file "+ inputfile + " could not be opened!")

    for line in inputfile:
      if line[0] !='!':
          #  inputs.append(line.strip().split('='))
          if line.strip().split('=')[0] == 'path_to_datafile':
              path_to_datafile = line.strip().split('=')[1]
          if line.strip().split('=')[0] == 'path_to_controlsfile':
              path_to_controlsfile = line.strip().split('=')[1]
          if line.strip().split('=')[0] == 'fish_per_cond':
              fish_per_cond = int(line.strip().split('=')[1])  
          if line.strip().split('=')[0] == 'conds':
              conds = int(line.strip().split('=')[1])   
          if line.strip().split('=')[0] == 'labels':
              labels = line.strip().split('=')[1].split(',')
          if line.strip().split('=')[0] == 'starttime':
              starttime = int(line.strip().split('=')[1])
          if line.strip().split('=')[0] == 'endtime':
              endtime = int(line.strip().split('=')[1])
          if line.strip().split('=')[0] == 'datagroups':          
              datagroups = []
              for each in line.strip().split('=')[1].split(';'):
                  datagroups.append([int(s) for s in each.split(',')]) 
          if line.strip().split('=')[0] == 'controlgroups':          
              controlgroups = []
              for each in line.strip().split('=')[1].split(';'):
                  controlgroups.append([int(s) for s in each.split(',')])        
    try:
        path_to_datafile
    except:
        print("No data file specified! Check " + path_to_inputfile)
    try:
        path_to_controlsfile
    except:
        print("No controls file specified. Assuming that control conditions are within " + path_to_datafile)
        path_to_controlsfile = path_to_datafile
    try:
        fish_per_cond
    except:
        print("fish_per_cond not specified! Check " + path_to_inputfile)
    try:
        conds
    except:
        print("No data file specified! Check " + path_to_inputfile)
    try:
        labels
    except:
        print("You have not specified any condition labels for plots. To input plot labels, change labels in" + path_to_inputfile)
     #   labels = [str(i) if i>0 and i<conds+1 else ' ' for i in range(0,conds+2)] 
        labels = [str(i+1) for i in range(0,conds)] 
    try:
        starttime
    except:
        sys.exit("Please specify starttime in " + path_to_inputfile)
    try:
        endtime
    except:
        sys.exit("Please specify endtime in " + path_to_inputfile)
    try:
        datagroups
    except:
        print("No datagroups specified; plotting all %i conditions independently" % conds)
        datagroups = [[int(i)] for i in range(0,conds)] 
        labels = [str(i+1) for i in range(0,conds)] 
    try:
        controlgroups
    except:
        print("No controlgroups specified; using first condition as a control")
        controlgroups = [0] 
    return [path_to_datafile, path_to_controlsfile, fish_per_cond, conds,starttime,endtime,datagroups,controlgroups,labels]

        
def readDataFile(path,**kwargs):
    if 'quiet' in kwargs:
        quiet = kwargs.get('quiet')  
    else:    
        quiet = False
        
    datafile = open(path,encoding='utf-16')
    data = []
    start = -97
    stepcount = 1
    nsteps = 0
    stepdata = {}
    steps = []
    
    datastart = 1e10
    dataend = -1e10
    
    for i,line in enumerate(datafile):
        line = line.replace('\x00', '')
        # Read field headers from first line of file
        if i==0: 
            fields = line.split('\t')
            if len(fields) == 49:
                print("Data file uses type 1 formatting with %i fields" % len(fields))
            elif len(fields) == 56:
                print("Data file uses type 2 formatting with %i fields" % len(fields))
            else: 
                print("Data file uses unknown formatting with %i fields. Known data file types have either 49 or 56 fields" % len(fields))
        
        # For every other line of file, read in data
        elif len(line.split('\t')) > 2:
            if len(fields) == 49:
                location = line.split('\t')[2]       
                # Reset necessary variables for each step in the data
                if int(float(line.split('\t')[8])) != start:
                    start = float(line.split('\t')[8])  
                    end = float(line.split('\t')[9])
                    if int(start) >= starttime and int(end) <= endtime+1:
                        steps.append({'start':start,'end':end})
                        if stepcount > 1:
                            data.append(stepdata)
                        stepdata = {}
                        stepcount = stepcount+1   
    
                    if start < datastart:
                        datastart = start
                    if end > dataend:
                        dataend = end
                    nsteps = nsteps + 1
                        
                if location not in stepdata:             
                    stepdata[location] ={'start': start,
                                          'end':float(line.split('\t')[9]), 
                                          'inact':int(line.split('\t')[16]), 
                                          'inadist':float(line.split('\t')[18]), 
                                          'smlct':int(line.split('\t')[19]), 
                                          'smldist':float(line.split('\t')[21]), 
                                          'larct':int(line.split('\t')[22]), 
                                          'lardist':float(line.split('\t')[24]) }  
            else:

                location = line.split('\t')[2]       
                # Reset necessary variables for each step in the data
                if int(float(line.split('\t')[8])) != start:
                    start = float(line.split('\t')[8])  
                    end = float(line.split('\t')[9])
                    if int(start) >= starttime and int(end) <= endtime+1:
                        steps.append({'start':start,'end':end})
                        if stepcount > 1:
                            data.append(stepdata)
                        stepdata = {}
                        stepcount = stepcount+1   
    
                    if start < datastart:
                        datastart = start
                    if end > dataend:
                        dataend = end
                    nsteps = nsteps + 1
                        

                if location not in stepdata:             
                    stepdata[location] ={'start': start,
                                          'end':float(line.split('\t')[9]), 
                                          'inact':int(line.split('\t')[22]), 
                                          'inadist':float(line.split('\t')[24]), 
                                          'smlct':int(line.split('\t')[25]), 
                                          'smldist':float(line.split('\t')[27]), 
                                          'larct':int(line.split('\t')[28]), 
                                          'lardist':float(line.split('\t')[30]) }  
    data.append(stepdata)

    if quiet==False:
        for j in np.arange(0,len(fields)):
            print("   %i - " % j + fields[j])
        print("   File spans %i - %i s with %i steps detected" % (datastart,dataend, nsteps))
        print("   Selected time range is %i - %i s (%i time steps)" % (steps[0]['start'],steps[-1]['end'],stepcount-1))
    datafile.close    
    return [data,steps]

def parseData(data,step):
    data_out = np.zeros([fish_per_cond,conds])
   # print(data[step])
    for fish in data[step]:
        # divide fish ID number by number of fish per condition and round 
        # to get fish number within condition
  #      fish_num = int((int(re.findall(r'\d+', fish)[0])-1)/conds)
        # use modulo division to find the condition number
   ##     cond = (int(re.findall(r'\d+', fish)[0])-1)%conds + 1  
        row = int((int(re.findall(r'\d+', fish)[0])-1)/12)
        col = (int(re.findall(r'\d+', fish)[0])-1)%12 + 1  
        
        ncols = int(12/conds) # number of columns to include in each condition
        
        fish_num = row + (col+1)%ncols*8
        cond = int((col+1)/ncols)-1
        data_out[fish_num, cond-1] = (data[step][fish]['inadist'] + 
                                       data[step][fish]['smldist'] + 
                                       data[step][fish]['lardist'])
    if np.product(np.shape(data_out)) - conds*fish_per_cond !=0:
        sys.exit("%i fish have been counted in the file but %i are expected" %(np.product(np.shape(data_out)),conds*fish_per_cond))
    return data_out

def doStats(data,**kwargs): 
    if 'control' in kwargs:
        control = kwargs.get('control')  
    else:
        control = data   
    if 'normtest' in kwargs:
        normtest = bool(kwargs.get('normtest'))
    else:
        normtest=False 
        

    mean_vals = np.mean(data,axis=0)
    stderror = stats.sem(data,axis=0)
    stdev = np.std(data,axis=0)
    
    numgroups=np.shape(data)[-1]
    
  
    Tvals = np.zeros(numgroups)
    pvals = np.zeros(numgroups)
    Fvals = np.zeros(numgroups)
    pvals_ANOVA = np.zeros(numgroups)
    shapiro = np.zeros(numgroups)
    pvals_shapiro = np.zeros(numgroups)
    for col in np.arange(0, numgroups):

        if numgroups > 2:
            if col > 1:    ################ change back to 1 for old setup  
                if col%2 ==0: # case for uninjured fish (even columns)
                    [Tvals[col], pvals[col]] = stats.ttest_ind(data[data[:,col]!=0,col],control[control[:,0]!=0,0])
                    [Fvals[col], pvals_ANOVA[col]] = stats.f_oneway(data[data[:,col]!=0,col],control[control[:,0]!=0,0])
                else: # case for injured fish (odd columns)   
                    [Tvals[col], pvals[col]] = stats.ttest_ind(data[data[:,col]!=0,col],control[control[:,1]!=0,1])                      
                    [Fvals[col], pvals_ANOVA[col]] = stats.f_oneway(data[data[:,col]!=0,col],control[control[:,0]!=0,1])
            #    [Tvals[0,col], pvals[0,col]] = stats.ttest_ind(data[:,col],control[:,0])                      
            #    [Fvals[0,col], pvals_ANOVA[0,col]] = stats.f_oneway(data[:,col],control[:,0])
        else:
            [Tvals[col], pvals[col]] = stats.ttest_ind(data[:,col], control[:,0])                      
            [Fvals[col], pvals_ANOVA[col]] = stats.f_oneway(data[:,col], control[:,0])         
        
        if normtest:
            mirrored_data = np.hstack((-data[col],data[col]))
            [shapiro[col],pvals_shapiro[col]] = stats.shapiro(data[col])
            fig = plt.figure(figsize=(8.5,3.5))
            ax1 = fig.add_subplot(121)
            y,x,_ =ax1.hist(data[data[:,col]!=0,col],color=colors[col])
            xvals = np.linspace(plt.gca().get_xlim()[0],plt.gca().get_xlim()[1],100)
            mu,std=stats.norm.fit(data[col])
            ax1.plot(xvals,stats.norm.pdf(xvals,mu,std),'k')
         
            ax2 = fig.add_subplot(122)
            ax2.axis('equal')
            qqplot(data[col],line='s',ax=ax2,color=colors[col])
            fig.tight_layout()
            title=fig.suptitle(labels[col] + "\nShapiro-Wilk test: %3.2f; p:%3.2e" % (shapiro[col],pvals_shapiro[col]),
                         y=1.1)
            figname = path_to_output + "/" + labels[col] + "_normtest.png"

          #  fig.savefig(figname,dpi=400,bbox_include_extra_artists=(title), bbox_inches='tight')

    return {'mean_vals':mean_vals, 
            'stdev':stdev, 
            'stderror': stderror, 
            'Tvals':Tvals, 
            'pvals':pvals,
            'Fvals':Fvals, 
            'pvals_ANOVA':pvals_ANOVA}

def writeData(data,step,start,end,stats_raw,stats_adj):
    conds=np.shape(data)[-1]
    # output data to text files 
#    filename = (path_to_datafile[:-4]+ "_" + 
#                str(start).zfill(4) + "-" + 
#                str(end).zfill(4) + ".txt")
    filename = (path_to_output + "/" + path_to_output.split('/')[-1] + "_" + 
                str(start).zfill(4) + "-" + 
                str(end).zfill(4) + ".txt")

    print("Printing file to %s" % filename)

    outfile = open(filename,'w+')
    
    outfile.write((5*" <><" + " FishStats v4.2 " + 5*"><> ").center(conds*10+7, '~')) 
    outfile.write("\n")


    outfile.write("Data from %s\n" % path_to_datafile)
    outfile.write("Total swimming distances (inadist + smldist + lardist) in mm\n")
    outfile.write("Time period: start=%i s, end=%i s\n" % (start,end) )
    outfile.write("Conditions are as follows:\n")
    for ii in np.arange(0,conds):
        outfile.write(("Cndtn" + str(ii+1).zfill(2)) +": " + labels[ii].strip() + "\n")
    outfile.write(" <>< ~ ><> ".center(conds*10+7,'~'))
    outfile.write("\nCONDTN:   ")
    for ii in np.arange(0,conds):
        outfile.write(("Cndtn" + str(ii+1).zfill(2)).ljust(11))
    outfile.write("\n")
    
    outfile.write("Period Stats (incl. zero values)".center(conds*10+10,'=')) 
    outfile.write("\nMEANVL: ")            
    np.savetxt(outfile,np.reshape(stats_raw['mean_vals'], [1,conds]), fmt="%10.6f")
    outfile.write("STDERR: ")
    np.savetxt(outfile,np.reshape(stats_raw['stderror'], [1,conds]), fmt="%10.6f")
    outfile.write("STDDEV: ")
    np.savetxt(outfile,np.reshape(stats_raw['stdev'], [1,conds]), fmt="%10.6f")
    outfile.write("T-test (vs. DMSO)".center(conds*10+10,'-')) 
    outfile.write("\n")
    outfile.write("TVALUE: ")
    np.savetxt(outfile,np.reshape(stats_raw['Tvals'], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(stats_raw['pvals'], [1,conds]), fmt="%10.2e")
    outfile.write("ANOVA (vs. DMSO)".center(conds*10+10,'-')) 
    outfile.write("\n")
    outfile.write("FVALUE: ")
    np.savetxt(outfile,np.reshape(stats_raw['Fvals'], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(stats_raw['pvals_ANOVA'], [1,conds]), fmt="%10.2e")
    
    outfile.write("Period Stats (excl. zero values)".center(conds*10+10,'=')) 
    outfile.write("\nMEANVL: ")            
    np.savetxt(outfile,np.reshape(stats_adj['mean_vals'], [1,conds]), fmt="%10.6f")
    outfile.write("STDERR: ")
    np.savetxt(outfile,np.reshape(stats_adj['stderror'], [1,conds]), fmt="%10.6f")
    outfile.write("STDDEV: ")
    np.savetxt(outfile,np.reshape(stats_adj['stdev'], [1,conds]), fmt="%10.6f")
    outfile.write("T-test (vs. cndtn 1)".center(conds*10+10,'-')) 
    outfile.write("\n")
    outfile.write("TVALUE: ")
    np.savetxt(outfile,np.reshape(stats_adj['Tvals'], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(stats_adj['pvals'], [1,conds]), fmt="%10.2e")

    outfile.write("ANOVA (vs. cndtn 1)".center(conds*10+10,'-')) 
    outfile.write("\n")
    outfile.write("FVALUE: ")
    np.savetxt(outfile,np.reshape(stats_adj['Fvals'], [1,conds]), fmt="%10.6f")
    outfile.write("PVALUE: ")
    np.savetxt(outfile,np.reshape(stats_adj['pvals_ANOVA'], [1,conds]), fmt="%10.2e")
    
    outfile.write("Detailed Values".center(conds*10+10,'='))
    outfile.write("\n")
    for ii, row in enumerate(data):
        outfile.write("FISH_"+str(ii+1).zfill(2)+":")
        np.savetxt(outfile,np.reshape(row, [1,conds]), fmt="%10.6f")
    outfile.close()
    
def plotData(data,datastats,step,start,end, **kwargs):   
    
    if 'control' in kwargs:
        control = kwargs.get('control')  
    else:
        control = data
    if 'controlstats' in kwargs:
        controlstats = kwargs.get('controlstats')  
    else:
        controlstats = datastats  
    if (int('controlstats' in kwargs) + int('control' in kwargs))%2 !=0:
        print("Warning: mismatch between control data and control statistics in plotting function")
    if 'labels' in kwargs:
        labels = kwargs.get('labels')  
    else:
        labels = [str(i) for i in np.arange(1,np.shape(data)[-1])]
    ngroups = np.shape(data)[-1]
    fish_per_group = np.shape(data)[0]
    conds=ngroups
    print("NGROUPS: %i" % ngroups)

    fig = plt.figure(figsize = (ngroups/2, 8))
    ax1 = fig.add_subplot(111)
    

   # print(datastats['mean_vals'])
    ax1.bar(1, controlstats['mean_vals'][0],yerr=controlstats['stderror'][0], width = 0.8,
            edgecolor='k', 
            color=[(0.3,0.3,0.3,1.0)],
            error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))
    ax1.bar(2, controlstats['mean_vals'][1],yerr=controlstats['stderror'][1], width = 0.8,
            edgecolor='k', 
            color=[(0.6,0.6,0.6,1.0)],
            error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))

    for i in np.arange(3, ngroups+2,2):

        ax1.bar((i+3)/2, datastats['mean_vals'][i-3],yerr=datastats['stderror'][i-3], width = 0.8,
                    edgecolor='k', 
                    color=['none' if (i-3)%2==0 else (0.9,0.9,0.9,1.0)],
                    error_kw=dict(ecolor='k', lw=1, capsize=5, capthick=1))

        data[data==0] = np.nan
        control[control==0] = np.nan
        
        offset = 0.1*(datastats['mean_vals'][i-3]+datastats['stderror'][i-3])
        
        if datastats['pvals'][i-3] < 0.05:
            ax1.annotate("*",((i+3)/2,datastats['mean_vals'][i-3]+datastats['stderror'][i-3]+offset),ha='center',fontsize=12)
            offset = 2*offset
        if datastats['mean_vals'][i-3]/controlstats['mean_vals'][1] >= 0.8:
            ax1.annotate("#",((i+3)/2,datastats['mean_vals'][i-3]+datastats['stderror'][i-3]+offset),ha='center',fontsize=12)

    #    ax1.plot(i + (np.random.rand(fish_per_group,1)-0.5)*0.75, data[:,i-1],markers[i-1],color=colors[i-1],markersize=4)

    ax1.set_xlim([0,(conds+2)/2+2])
    ax1.set_xticks(np.arange(1,int(ngroups+3)/2+1))
    xticks = ax1.set_xticklabels(np.hstack((labels[:2],labels[3::2])) ,rotation=60)
    ax1.set_title("Time Period: (%i s - %i s)" % (start, end))
  #  ax1.set_xlabel("Condition")
    ax1.set_ylabel("Cumulative Distance (cm)")
   # figname = path_to_datafile[:-4] +"_" + str(start).zfill(4) + "-" + str(end).zfill(4) + ".png"
    figname = path_to_output + "/" + path_to_output.split('/')[-1]  +"_" + str(start).zfill(4) + "-" + str(end).zfill(4) + ".png"
    print("Saving figure to " + figname)
    fig.savefig(figname,dpi=400,bbox_include_extra_artists=(xticks), bbox_inches='tight')

def plotTimeSeries(time,data,labels):
    plottime = endtime-starttime
    endpoint = np.argmin(abs(time - endtime ))
   # figname = path_to_datafile[:-4] +"_" + str(starttime).zfill(4) + "-" + str(endtime).zfill(4)
    figname = path_to_output + "/" + path_to_output.split('/')[-1]  +"_" + str(starttime).zfill(4) + "-" + str(endtime).zfill(4)
       
    # plot time series of cumulative data
    fig1 = plt.figure(figsize=(10,6))
    ax1 = fig1.add_subplot(111)
    

    
 #   colors = pl.cm.viridis(np.linspace(0,0.9,len(datagroups)))
    ax1.text(0.75,0.05,"Temporal resolution: %i s" % np.mean(np.diff(time)),transform=ax1.transAxes)
    
    for i in np.arange(0,np.shape(data)[-1]):
        series = data[:,:,i]
        if fig1.get_size_inches()[0]*fig1.dpi/np.shape(series)[-1] < 10:  # threshold for turning markers on/off to make plots more legible.
            marker =  ''
        else:
            marker= markers[i]
        ax1.plot(time-time[0],np.cumsum(np.mean(series,axis=0)),label=labels[i],color=colors[i],marker=marker)
    ax1.legend()
    ax1.set_xlabel("Time (s)")
    ax1.set_ylabel("Cumulative distance swum (cm)")
    ax1.set_xlim([0,time[endpoint]-time[0]])
    ax1.set_ylim(bottom=0)

    
    fig2 = plt.figure(figsize=(10,6))
    ax2 = fig2.add_subplot(111)
    
    ax2.text(0.75,0.05,"Temporal resolution: %i s" % np.mean(np.diff(time)),transform=ax2.transAxes)    

    for i in np.arange(0,np.shape(data)[-1]):
        series = data[:,:,i]
        if fig1.get_size_inches()[0]*fig1.dpi/np.shape(series)[-1] < 10:  # threshold for turning markers on/off to make plots more legible.
            marker =  ''
        else:
            marker= markers[i]
        ax2.plot(time-time[0],np.mean(series,axis=0),label=labels[i],color=colors[i],marker=marker)
    ax2.legend()
    ax2.set_xlabel("Time (s)")
    ax2.set_ylabel("Distance swum (cm)")
    ax2.set_xlim([0,time[endpoint]-time[0]])
    ax2.set_ylim(bottom=0)


    fig1.savefig(figname+"_cumulative_vs_time.png",dpi=500)    
    fig2.savefig(figname+"_timeseries.png",dpi=500)
    
def plotContours(data,start,end):
    conds=len(datagroups)
#    figname = path_to_datafile[:-4] +"_" + str(starttime).zfill(4) + "-" + str(endtime).zfill(4)
    figname = path_to_output + "/" + path_to_output.split('/')[-1] +"_" + str(starttime).zfill(4) + "-" + str(endtime).zfill(4)

    fig3 = plt.figure(figsize=(12,8))
    ax3 = fig3.add_subplot(111)

    mat = ax3.matshow(data)
    for y in np.arange(-0.5, np.shape(data)[0]+1-0.5):
        ax3.axhline(y, lw=2, color='k', zorder=5)
        
    for x in np.arange(-0.5, np.shape(data)[1]+1-0.5):
        ax3.axvline(x, lw=2, color='k', zorder=5)
        

    
    divider = make_axes_locatable(ax3)
    cax = divider.append_axes("right", size="5%", pad=0.15)
    bar = fig3.colorbar(mat,cax=cax)
    ax3.set_xticks(np.arange(0,conds+1))
    ax3.set_xticklabels([str(i+1) for i in np.arange(0,conds)])
    ax3.set_yticks(np.arange(fish_per_cond-1,-1,-1))
    ax3.set_yticklabels([str(i+1) for i in np.arange(0,fish_per_cond)])
  #  ax3.invert_yaxis()
    
    ax3.set_xlim(-0.5,11.5)
    ax3.set_ylim(-0.5, 7.5)
    bar.set_label("Cumulative distance swum (cm)")
    ax3.set_title("Time Period: (%i s - %i s)" % (start, end))

    fig3.savefig(figname+"_plate_contours2.png",dpi=500)    
    
def vomitData(data,step):
    for fish in data[step]:
        fish_num = int((int(re.findall(r'\d+', fish)[0]))/conds)
        # use modulo division to find the condition number
        cond = (int(re.findall(r'\d+', fish)[0])-1)%conds + 1 
        print(">%s - Condition %i, Fish %i (%i-%i) inadist: %2.3f smldist: %2.3f lardist: %2.3f totaldist: %2.3f" % 
                                                     (fish, cond,fish_num,
                                                     data[step][fish]['start'], 
                                                     data[step][fish]['end'],
                                                     data[step][fish]['inadist'],
                                                     data[step][fish]['smldist'],
                                                     data[step][fish]['lardist'],
                                                     data[step][fish]['inadist']+data[step][fish]['smldist']+data[step][fish]['lardist']))


def selectDataFiles():
    root = tk.Tk()
    root.lift()
    root.withdraw()
    
    file_paths = tk.filedialog.askopenfilename(multiple=True, title="Select data files",parent=root)
    root.destroy()
    
    datafiles=[]
    controlfiles=[]
    for path in file_paths:
        file = path.split('/')[-1]
        ##for chunk in file.split('-'):
        if 'DMSO' in file.split('-'):
            print("This file contains a control:",file)
            controlfiles.append(path)
        else:
            print("This contains no control:",file)
            datafiles.append(path)
            

    return(file_paths)
    
if __name__ == "__main__":
    print("Welcome to FishStats V5.0!")   
    vomit = False
    
    if len(sys.argv) > 1 and sys.argv[1] != '-v':
        path_to_inputfile = sys.argv[1]
    else:
        path_to_inputfile = "fishstats_inputs.txt"
       
    if sys.argv[-1] == '-v':
        vomit = True
        
    [path_to_datafile, 
     path_to_controlsfile, 
     fish_per_cond, 
     conds,
     starttime,
     endtime,
     datagroups,
     controlgroups,
     labels] = openInputFile(path_to_inputfile)  


    files = selectDataFiles()


    if os.path.isdir("Output") == False:
        os.mkdir("Output")

    path_to_output = "Output/" + path_to_datafile.split('/')[-1][:-4]
    if os.path.isdir(path_to_output) == False:
        os.mkdir(path_to_output)
    print("Creating output directory: " + path_to_output)


    print("There are %i test conditions with %i fish per condition." % (conds, fish_per_cond))
    if len(datagroups) < conds:
        print("The following conditions are being grouped together:")
        for i in np.arange(0,len(datagroups)):
            print(str(datagroups[i]))
    
    
    
  #  colors = pl.cm.plasma(np.linspace(0.75,0.,len(datagroups)))
 #   colors[1] = (0,0,0,1)
  #  colors[0] = (0.7,0.7,0.7,1.0)
    # 332288, 88CCEE, 44AA99, 117733, 999933, DDCC77, CC6677, 882255, AA4499
    
    # color map from colorbrewer2
    colors = ['#000000','#666666','#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#a6761d'] #'#e6ab02'
    
             
    markers = ['o','v','s','H','D','X','P','^','X','+',]  
        
    
    main()

