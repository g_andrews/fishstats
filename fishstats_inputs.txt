! This is the path to the main datafile
path_to_datafile=../Data/E3-Ringer-1-21-22.txt

! This is the path to the control datafile (OPTIONAL)
!!path_to_controlfile=1dpiLightStartle-4-26-21.xls.txt

! Number of conditions
conds=12

! Number of fish in each condition
fish_per_cond=8

! Start time of relevant time interval (in seconds)
starttime= 1800

! End time
endtime=1805

! Groupings of different conditions (numbers correspond to columns on plate - 0 is first, 1 is second, etc.)
datagroups=0,1;2,3;4,5;6,7;8,9;10,11

! Groups to use as control data
controlgroups=0,1;2,3

! Labels for each group to be used for plotting
labels=1,2,3,4,5,6